	$(".js-height-full").height($(window).height());
	$(".js-height-parent").each(function() {
	    $(this).height($(this).parent().first().height());
	});


	// Fun Facts
	function count($this) {
	    var current = parseInt($this.html(), 10);
	    current = current + 1; /* Where 50 is increment */

	    $this.html(++current);
	    if (current > $this.data('count')) {
	        $this.html($this.data('count'));
	    } else {
	        setTimeout(function() {
	            count($this)
	        }, 50);
	    }
	}

	$(".stat-timer").each(function() {
	    $(this).data('count', parseInt($(this).html(), 10));
	    $(this).html('0');
	    count($(this));
	});



	$('.header').affix({
	    offset: {
	        top: 100,
	        bottom: function() {
	            return (this.bottom = $('.footer').outerHeight(true))
	        }
	    }
	})

	$(window).load(function() {
	    $("#preloader").on(500).fadeOut();
	    $(".preloader").on(600).fadeOut("slow");
	});


function cargarProgramas(){

	var suma	= $("#txtSuma").val();
	var total	= $("#txtTotal").val();

	if(suma >= total){
		return;
	}

	$.post("gestion/modulos/presencial/web.php?cmd=cargarProgramas", {
        suma: suma
    }, function(data) {
        $("#ContenedorListado").html(data);
		
    });
}

function cargarComunidad(){

	$.post("gestion/modulos/presencial/web.php?cmd=cargarComunidad", {
    }, function(data) {
        $("#ContenedorListado").html(data);
		
    });
}

function cargarAlianza(){

	$.post("gestion/modulos/presencial/web.php?cmd=cargarAlianza", {
    }, function(data) {
        $("#ContenedorListado").html(data);
		
    });
}