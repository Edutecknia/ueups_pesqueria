<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = '';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($campo,$orden,$direccion,$pagina,$estado,$filtro){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelUsuario 	= new Data_sgausuario();
	$arrayUsuario 	= $modelUsuario->fu_Listar($conexion,$idSociedad, $filtro,$estado,$pagina);

	foreach ($arrayUsuario as $obj):
        $numTotalElementos = $obj['FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var orden = $("#orden").val();
							var direccion = $("#direccion").val();
							var valor = $("#txtBuscar").val();
							var campo = "";
							var estado=$("#cboEstado").val();
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/usuario/usuario.php?cmd=listar", {
						orden:orden, direccion:direccion, valor:valor,pagina:num,estado:estado,campo:campo
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelUsuario 			= new Data_sgausuario();
	$arrayTipoDocumento 	= $modelUsuario->fu_listarTipoDocumento($conexion);

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($nombreUsuario,$apellidoUsuario,$idTipoDocumento,$numDocumento,$loginUsuario,$mailUsuario){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_usuarioRegistrar($conexion,$nombreUsuario,$apellidoUsuario,$loginUsuario,$idTipoDocumento,$numDocumento,$mailUsuario,$idUsuario); 

	if($rpta=='2'){
		echo 'passed';
	}
	else{ 
		if($rpta=='1'){
		echo 'El nombre de usuario ya existe';
		}
	else{
		if($rpta=='0'){
		echo 'El número de documento ya existe';
		}
	else{
		echo 'Error al registrar, vuelva a intentarlo';
	}
	}
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelUsuario 			= new Data_sgausuario();
	$arrayTipoDocumento 	= $modelUsuario->fu_listarTipoDocumento($conexion);
	
	$obj_Usuario 		= new Data_sgausuario();
	$obj_Usuario 		= $obj_Usuario->fu_Encontrar($conexion,$id);
	$id = $id;

	include $nombreModulo . 'vistas/editar.php';

}

function editar($id, $nombreUsuario,$apellidoUsuario,$idTipoDocumento,$numDocumento,$loginUsuario,$mailUsuario){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_usuarioEditar($conexion,$id,$nombreUsuario,$apellidoUsuario,$loginUsuario,$idTipoDocumento,$numDocumento,$mailUsuario,$idUsuario); 

	if($rpta=='2'){
		echo 'passed';
	}
	else{ 
		if($rpta=='1'){
		echo 'El nombre de usuario ya existe';
		}
	else{
		if($rpta=='0'){
		echo 'El número de documento ya existe';
		}
	else{
		echo 'Error al registrar, vuelva a intentarlo';
	}
	}
	}
	
}

function loadVisualizar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$modelUsuario 			= new Data_sgausuario();
	$arrayTipoDocumento 	= $modelUsuario->fu_listarTipoDocumento($conexion);
	
	$obj_Usuario 		= new Data_sgausuario();
	$obj_Usuario 		= $obj_Usuario->fu_Encontrar($conexion,$id);

	$id = $id;
	$indVisualizar = 1;
	include $nombreModulo . 'vistas/editar.php';

}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_usuarioInactivar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_usuarioActivar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}

function loadPermisos($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelUsuario 	= new Data_sgausuario();
	$arrayPermisos  = $modelUsuario->fu_permisoListar($conexion,$id); 

	$array=array();
	if(count($arrayPermisos)>0){

		foreach ($arrayPermisos as $obj):

		 $array[] = $obj['ID_SISTEMA_PRIVILEGIO'];

		endforeach;

	}
	
	$id = $id;

	include $nombreModulo . 'vistas/permiso.php';
}

function registrarPermiso($id,$cadena){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();
	$rpta_ =$modelUsuario->fu_permisoEliminar($conexion,$id); 

	if($cadena != ''){

		$data = explode(',', $cadena);

		for ($i=0; $i < count($data); $i++) { 
			
			$rpta =$modelUsuario->fu_permisoRegistrar($conexion,$id,$data[$i],$idUsuario);

		}

		if($rpta > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}

	}
	else{
		if($rpta_ > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}
	}

}

function loadClave($id){
	$id = $id;
	include $nombreModulo . 'vistas/clave.php';
}

function editarClave($id,$clave){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();
	$rpta =$modelUsuario->fu_editarClave($conexion,$id,$clave,$idUsuario);

		if($rpta > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}

}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['campo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['estado'],$_POST['valor']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['nombreUsuario'],$_POST['apellidoUsuario'],$_POST['idTipoDocumento'],$_POST['numDocumento'],$_POST['loginUsuario'],$_POST['mailUsuario']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;
	case 'editar':
		editar($_POST['id'],$_POST['nombreUsuario'],$_POST['apellidoUsuario'],$_POST['idTipoDocumento'],$_POST['numDocumento'],$_POST['loginUsuario'],$_POST['mailUsuario']);break;
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'loadDetalle':
		loadDetalle($_POST['id']);break;
	case 'registrardetalle':
		registrardetalle($_POST['idPrograma'],$_POST['contenido']);break;
	case 'loadPermisos':
		loadPermisos($_POST['id']);break;
	case 'registrarPermiso':
		registrarPermiso($_POST['id'],$_POST['cadena']);break;
	case 'loadClave':
		loadClave($_POST['id']);break;	
	case 'editarClave':
		editarClave($_POST['id'],$_POST['con1']);break;	
    default:
        vistaPrincipal();
        break;
}
?>