<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th style="text-align:center">Nombre</th>
                        <th class="center" style="text-align:center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayPrograma as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <td><?php echo $obj['NOM_GALERIA'];?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar" 
                        onclick="javascript:OpenForm('edicion','<?php echo $obj['ID_GALERIA'] ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Activo - Click para inactivar" 
                        onclick="javascript:Inactivar('<?php echo $obj['ID_GALERIA'] ?>');">
                        <i class="fa fa-check-square-o"></i>
                    </a>
                    <?php
                    }
                    else{
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Inactivo - Click para activar" 
                        onclick="javascript:Activar('<?php echo $obj['ID_GALERIA'] ?>');">
                        <i class="fa fa-power-off"></i>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  