<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Orden de Ubicación</th>
                        <th>Ubicación Slider</th>
                        <th class="center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayPrograma as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <input type="hidden" name="<?php echo $obj['ID_PROGRAMA'] ?>" id="<?php echo $obj['ID_PROGRAMA'] ?>" value="<?php echo $obj['NOM_PROGRAMA'];?>" />
                    <td><?php echo $obj['NOM_PROGRAMA'];?></td>
                    <td class="right"><?php echo $obj['ORDEN_PROGRAMA'];?></td>
                    <td class="right"><?php if($obj['SLIDER'] == 1){ echo 'Slider Superior';}else{ echo 'Slider Inferior';} ;?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar" 
                        onclick="javascript:OpenForm('edicion','<?php echo $obj['ID_PROGRAMA'] ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm('visualizar','<?php echo $obj['ID_PROGRAMA'] ?>');">
                        <i class="fa fa-eye"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Activo - Click para inactivar" 
                        onclick="javascript:Inactivar('<?php echo $obj['ID_PROGRAMA'] ?>');">
                        <i class="fa fa-check-square-o"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Eliminar" 
                        onclick="javascript:eliminar('<?php echo $obj['ID_PROGRAMA'] ?>');">
                        <i class="fa fa-trash"></i>
                    </a>
                    <?php
                    }
                    else{
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Inactivo - Click para activar" 
                        onclick="javascript:Activar('<?php echo $obj['ID_PROGRAMA'] ?>');">
                        <i class="fa fa-power-off"></i>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  