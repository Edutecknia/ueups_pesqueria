// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('curso');rutaMenu('Cursos','Front Desk','Cursos');
}

function listado(){
	var orden = $("#orden").attr("value");
    var direccion=$("#direccion").attr("value");
    var valor = $("#txtBuscar").val();
    var page = $("#pag_actual").attr("value");
	var campo = 'tiph_descripcion';
	var estado= $("#cboEstado").val();
	var idPrograma = $("#cboPrograma").val();
	INI_LOAD();
    $.post("modulos/curso/curso.php?cmd=listar", {
        orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		campo: campo, idPrograma:idPrograma
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd 		= "loadCreacion";
    var titulo 		= "";
    var programa 	= '';

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar nuevo curso";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar curso";
    }
    if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Curso";
    }
    
  
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/curso/curso.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarCurso(){
	var idPrograma 		= $("#cboPrograma").val();
	var nombre 			= $("#nombreCurso").val();
	var descripcion   	= $("#Descripcion").val();
	var contenido   	= $("#Contenido").val();
	var finalizar 		= $("#Finalizar").val();
	var precio 			= $("#txtPrecio").val();
	var duracion 		= $("#txtDuracion").val();
	var modalidad 		= $("#txtModalidad").val();
	var orden 			= $("#txtorden").val();
	var nomExpositor 	= $("#nombreExpositor").val();

		INI_LOAD();
    $.post("modulos/curso/curso.php?cmd=registrar", {
        idPrograma:idPrograma, nombre:nombre, descripcion:descripcion, contenido:contenido, finalizar:finalizar, precio:precio, duracion:duracion, 
        modalidad:modalidad, orden:orden,	nomExpositor:nomExpositor
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}


function editarCurso(){
	var idCurso			= $("#idCurso").val();
	var idPrograma 		= $("#cboPrograma").val();
	var nombre 			= $("#nombreCurso").val();
	var descripcion   	= $("#Descripcion").val();
	var contenido   	= $("#Contenido").val();
	var finalizar 		= $("#Finalizar").val();
	var precio 			= $("#txtPrecio").val();
	var duracion 		= $("#txtDuracion").val();
	var modalidad 		= $("#txtModalidad").val();
	var orden 			= $("#txtorden").val();
	var nomExpositor 	= $("#nombreExpositor").val();

		INI_LOAD();
    $.post("modulos/curso/curso.php?cmd=editar", {
        idCurso:idCurso, idPrograma:idPrograma, nombre:nombre, descripcion:descripcion, contenido:contenido, finalizar:finalizar, precio:precio, duracion:duracion, 
        modalidad:modalidad, orden:orden, nomExpositor:nomExpositor
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro modificado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al modificar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/curso/curso.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/curso/curso.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function OpenFormPDF(id)
{
    var cmd 		= "loadPDF";
    var titulo 		= "";
  
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/curso/curso.php?cmd=" + cmd, {id: id},
        function(data) {
			//$("#Contenedorform").html(data);
			$("#childModal1").html(data);
			//$("#titulo").html(titulo);
        });

        $("#childModal1").modal({
        });
    });
}

function subirPdf(){
	var idCurso 		= $("#txtid").val();

		INI_LOAD();
    $.post("modulos/curso/curso.php?cmd=subirpdf", {
        idCurso:idCurso
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			listado();			
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
			   QuitarModal();
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function eliminar(id){
	
	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/curso/curso.php?cmd=eliminar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro eliminado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al eliminar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}


