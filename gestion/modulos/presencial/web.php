<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

function cargarProgramas($suma){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	if($suma == null || $suma == ''){
		$suma = 0;
	}

	$suma 		= $suma + 8;

	$obj_sgaprograma = new Data_sgaprograma();
	$arrayProgramma  = $obj_sgaprograma->fu_listarPrograma($conexion,$suma);
	$totalPrograma 	 = $obj_sgaprograma->fu_totalPrograma($conexion);
	
	$total 	= count($totalPrograma);
	$suma 	= $suma;

	include $nombreModulo . 'vistas/web_programas.php';
}

function cargarComunidad(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$obj_sgaprograma = new Data_sgaprograma();
	$arrayProgramma  = $obj_sgaprograma->fu_listarComunidad($conexion);

	include $nombreModulo . 'vistas/web_comunidad.php';
}

function cargarAlianza(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$obj_sgaprograma = new Data_sgaprograma();
	$arrayProgramma  = $obj_sgaprograma->fu_listarAlianza($conexion);

	include $nombreModulo . 'vistas/web_alianza.php';
}



/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'cargarProgramas':
		cargarProgramas($_POST['suma']);break;
	case 'cargarComunidad':
		cargarComunidad();break;	
	case 'cargarAlianza':
		cargarAlianza();break;	
    default:
        vistaPrincipal();
        break;
}
?>