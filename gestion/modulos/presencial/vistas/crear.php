<script>
$(document).ready(function(){
 $('#dtfllegada').datetimepicker(
   {
     format: 'DD/MM/YYYY',
     defaultDate: new Date()
   }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
  });   
  
  //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
  
});
</script>

<link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/custompresencial.js?v=1.0.5"></script>

<form action="javascript:registrarPrograma();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">



  <div class="form-group col-sm-12">
                <label>Nombre del Programa:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombrePrograma" name="nombrePrograma" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div>

<div class="form-group col-sm-3">
    <label>Fecha:</label>
  <div class='input-group date' id='dtfllegada'>
<input type='text' class="form-control" id="nomCiudad"  />
                   <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                      </div>


<!--<div class="form-group col-sm-3">
                <label>Fecha:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomCiudad" name="nomCiudad" 
                                    class="form-control" 
                                    placeholder="Ingrese fecha del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div> -->


<div class="form-group col-sm-3">
                <label>Lugar:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtLugar" name="txtLugar" 
                                    class="form-control" 
                                    placeholder="Ingrese lugar del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div> 


<div class="form-group col-sm-3">
                <label>Horario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtHorario" name="txtHorario" 
                                    class="form-control" 
                                    placeholder="Ingrese horario del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div> 

<div class="form-group col-sm-3">
                <label>Horas del Curso:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDuracion" name="txtDuracion" 
                                    class="form-control" 
                                    placeholder="Horas de duración del programa" 
                                    type="number" step="1"
                                    maxlength="5" 
                                    required="required">
                                </div>
                        </div> 


<div class="form-group  col-sm-3">
                <label>Orden de Ubicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtorden" name="txtorden" class="form-control" placeholder="Ingrese valor" type="number" step="1" required="required">
                                </div>
                        </div>

<!--<div class="form-group col-sm-4">
    <label>Fecha Programa:</label>
  <div class='input-group date' id='dtfllegada'>-->  
<input type='hidden' class="form-control" id="fechaPrograma" required="required" />
                   <!-- <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                      </div>-->                                              


<!--<div class="form-group col-sm-12">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="desPrograma" name="desPrograma" 
                                    class="form-control" 
                                    placeholder="Descripción del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div> -->


<div class="tab-content col-sm-12" style="background-color:#FFF;">
      <div class="tab-pane fade active in" id="subirfoto">
      

         <div class="form-group col-sm-12">
         <center>
         <?php //echo $idcarpeta;?>
<input type="hidden" name="txtidcarpeta" id="txtidcarpeta" value="L0001"/>
    <label>Medidas(1920x1080px)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input2" multiple>
                                </div>
                                </center>
                      </div>


                      
                      
      </div>
      </div>

</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   