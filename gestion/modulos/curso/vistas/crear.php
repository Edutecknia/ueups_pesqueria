﻿<script>
$(document).ready(function(){
 $('#dtfllegada').datetimepicker(
   {
     format: 'DD/MM/YYYY',
     defaultDate: new Date()
   }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
  });   
  
  //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
  
});
</script>

<link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/customcurso.js?v=1.0.5"></script>

<form action="javascript:registrarCurso();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">


  <div class="form-group col-sm-6">
                <label>Programa:</label>
                                <select class="form-control" name="cboPrograma" id="cboPrograma">
                                <?php foreach ($arrayPrograma as $obj): ?>
                                <option value="<?php echo $obj['ID_PROGRAMA']; ?>"><?php echo $obj['NOM_PROGRAMA']; ?></option>
                                <?php endforeach; ?>
                                </select>
                        </div>

  <div class="form-group col-sm-6">
                <label>Curso:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="nombreCurso" name="nombreCurso" placeholder="Ingrese nombre del curso" required="required"></textarea>
                                </div>
                        </div>

<div class="form-group  col-sm-12">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Descripcion" name="Descripcion" placeholder="Ingrese descripción del curso" required="required"></textarea>
                                </div>
                        </div>   

<div class="form-group  col-sm-12">
                <label>Contenido:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Contenido" name="Contenido" placeholder="Ingrese contenido del curso" required="required"></textarea>
                                </div>
                        </div>  

<div class="form-group  col-sm-12">
                <label>Al Finalizar:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Finalizar" name="Finalizar" placeholder="Logros al finalizar el curso" required="required"></textarea>
                                </div>
                        </div>   

<div class="form-group col-sm-12">
                <label>Expositor:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreExpositor" name="nombreExpositor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre del expositor" 
                                    type="text" 
                                    maxlength="100" >
                                </div>
                        </div>                                                                    

<div class="form-group  col-sm-3">
                <label>Precio:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtPrecio" name="txtPrecio" class="form-control" placeholder="Ingrese valor" type="number" step="0.01" required="required">
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Duración:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDuracion" name="txtDuracion" class="form-control" placeholder="Duración en horas" type="number" step="1" required="required">
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Modalidad:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtModalidad" name="txtModalidad" class="form-control" placeholder="Ingrese valor" type="text" required="required">
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Orden de Ubicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtorden" name="txtorden" class="form-control" placeholder="Ingrese valor" type="number" step="1" required="required">
                                </div>
                        </div>

                                             
<div class="tab-content col-sm-12" style="background-color:#FFF;">
      <div class="tab-pane fade active in" id="subirfoto">
      

        <div class="form-group col-sm-4">
       <!-- <label>Imagen de Fondo (800x800px)</label>
        <div class="input-group">
        
        <input type="file" name="files[]" id="filer_input2" multiple>
        </div>-->
        </div>

        
        <div class="form-group col-sm-4">
        <label>Imagen de curso</label>
        <div class="input-group">
        
        <input type="file" name="files[]" id="filer_input2" multiple>
        </div>
        </div>


        <div class="form-group col-sm-4">
        <!--<label>Imagen Expositor (160x160px)</label>
        <div class="input-group">
        
        <input type="file" name="files[]" id="filer_input4" multiple>
        </div>-->
        </div>

                      
                      
      </div>
      </div>

</div>



                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					
            