<form action="javascript:editarUsuario();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

<input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  


  <div class="form-group col-sm-6">
                <label>Nombres:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreUsuario" name="nombreUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre del usuario" 
                                    type="text" 
                                    maxlength="50" 
                                    required="required"
                                    value="<?php echo $obj_Usuario['NOM_USUARIO']; ?>">
                                </div>
                        </div>

  <div class="form-group col-sm-6">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apellidoUsuario" name="apellidoUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese apellidos del usuario" 
                                    type="text" 
                                    maxlength="50" 
                                    required="required"
                                    value="<?php echo $obj_Usuario['APE_USUARIO']; ?>">
                                </div>
                        </div>                        


<div class="form-group col-sm-6">
                <label>Tipo Documento:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <select class="form-control" name="cboTipoDocumento" id="cboTipoDocumento">
                                <?php foreach ($arrayTipoDocumento as $obj): ?>
                                <option value="<?php echo $obj['ID_DOCUMENTO_IDENTIDAD']; ?>"
                                   <?php if($obj['ID_DOCUMENTO_IDENTIDAD'] == $obj_Usuario['ID_DOCUMENTO_IDENTIDAD'] ){ echo 'selected'; } ?>
                                ><?php echo $obj['ABR_DOCUMENTO_IDENTIDAD']; ?></option>
                                <?php endforeach; ?>
                                </select>
                                </div>
                        </div>  

<div class="form-group  col-sm-6">
                <label>Número Documento:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="numDocumento" name="numDocumento" 
                                    class="form-control" 
                                    placeholder="Ingrese número de documento de identidad" 
                                    type="text" 
                                    maxlength="10" 
                                    required="required"
                                    value="<?php echo $obj_Usuario['NUM_DOCUMENTO_IDENTIDAD']; ?>">
                                </div>
                        </div>
                                             


  <div class="form-group col-sm-6">
                <label>Usuario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="loginUsuario" name="loginUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese login de usuario" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_Usuario['NOM_LOGIN']; ?>">
                                </div>
                        </div> 

<div class="form-group col-sm-6">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailUsuario" name="mailUsuario" 
                                    class="form-control" 
                                    placeholder="E-mail del usuario" 
                                    type="text" 
                                    maxlength="100" 
                                    value="<?php echo $obj_Usuario['DES_EMAIL']; ?>">
                                </div>
                        </div> 


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<?php if($indVisualizar != 1){ ?>                        
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
<?php } ?>
                        </div>

 </div>
</form>         