﻿<script>
$(document).ready(function(){
 $('#dtfllegada').datetimepicker(
   {
     format: 'DD/MM/YYYY',
     defaultDate: new Date()
   }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
  });   
  
  //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
  
});
</script>

<link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/customcurso.js?v=1.0.5"></script>

<form action="javascript:editarCurso();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

    <input type="hidden"
            name="idCurso"
            id="idCurso"
            value="<?php echo $id; ?>"
    >

  <div class="form-group col-sm-6">
                <label>Programa:</label>
                                <select class="form-control" name="cboPrograma" id="cboPrograma">
                                <?php foreach ($arrayPrograma as $obj): ?>
                                <option value="<?php echo $obj['ID_PROGRAMA']; ?>"
                                <?php if($obj['ID_PROGRAMA'] == $objPrograma->__get('_ID_PROGRAMA') ){ echo 'selected'; } ?>
                                ><?php echo $obj['NOM_PROGRAMA']; ?></option>
                                <?php endforeach; ?>
                                </select>
                        </div>

  <div class="form-group col-sm-6">
                <label>Curso:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="nombreCurso" name="nombreCurso" required="required" placeholder="Ingrese nombre del curso"><?php echo $objPrograma->__get('_NOM_CURSO'); ?></textarea>                              </div>
                        </div>

<div class="form-group  col-sm-12">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Descripcion" name="Descripcion" required="required" placeholder="Ingrese descripción del curso"><?php echo $objPrograma->__get('_DES_CURSO'); ?></textarea>
                                </div>
                        </div>   

<div class="form-group  col-sm-12">
                <label>Contenido:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Contenido" name="Contenido" required="required" placeholder="Ingrese contenido del curso"><?php echo $objPrograma->__get('_CONTENIDO_CURSO'); ?></textarea>
                                </div>
                        </div>  

<div class="form-group  col-sm-12">
                <label>Al Finalizar:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control" id="Finalizar" name="Finalizar" required="required" placeholder="Logros al finalizar el curso"><?php echo $objPrograma->__get('_FINALIZAR_CURSO'); ?></textarea>
                                </div>
                        </div>                                                                       

<div class="form-group col-sm-12">
                <label>Expositor:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreExpositor" name="nombreExpositor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre del expositor" 
                                    type="text" 
                                    value="<?php echo $objPrograma->__get('_NOM_EXPOSITOR'); ?>"
                                    maxlength="100" >
                                </div>
                        </div>   

<div class="form-group  col-sm-3">
                <label>Precio:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtPrecio" 
                                            name="txtPrecio" 
                                            class="form-control" 
                                            placeholder="Ingrese valor" 
                                            type="number" 
                                            step="0.01" 
                                            required="required"
                                            value="<?php echo $objPrograma->__get('_PRECIO_CURSO'); ?>"
                                            >
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Duración:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDuracion" 
                                            name="txtDuracion" 
                                            class="form-control" 
                                            placeholder="Ingrese valor" 
                                            type="number" 
                                            step="1" 
                                            required="required"
                                            value="<?php echo $objPrograma->__get('_DURACION_CURSO'); ?>"
                                            >
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Modalidad:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtModalidad" 
                                            name="txtModalidad" 
                                            class="form-control" 
                                            placeholder="Ingrese valor" 
                                            type="text" 
                                            required="required"
                                            value="<?php echo $objPrograma->__get('_MODALIDAD'); ?>"
                                            >
                                </div>
                        </div>

<div class="form-group  col-sm-3">
                <label>Orden de Ubicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtorden" 
                                            name="txtorden" 
                                            class="form-control" 
                                            placeholder="Ingrese valor" 
                                            type="number" 
                                            step="1" 
                                            required="required"
                                            value="<?php echo $objPrograma->__get('_ORDEN_CURSO'); ?>"
                                            >
                                </div>
                        </div>


<div class="tab-content col-sm-12" style="background-color:#FFF;">
      <div class="tab-pane fade active in" id="subirfoto">
      

         <div class="form-group col-sm-12">
         <center>
         <input type="hidden" name="txtidcarpeta" id="txtidcarpeta" value="EDIT_<?php echo $id;?>"/>
          <label>Imagen de Curso (400x289px)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input2" multiple>
                                </div>
                                </center>
                      </div>


            
            <div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid">
            <li class="jFiler-item" data-jfiler-index="0" style="">                        
            <div class="jFiler-item-container">                            
            <div class="jFiler-item-inner">                                
            <div class="jFiler-item-thumb">                                    
            <div class="jFiler-item-status"></div>                                    
            <div class="jFiler-item-info">                                        
           <span class="jFiler-item-title"><b title="<?php echo $objPrograma->__get('_NOM_IMAGEN'); ?>"><?php echo $objPrograma->__get('_NOM_IMAGEN'); ?></b></span> 
            <span class="jFiler-item-others">KB</span>                                 
            </div>                                    
            <div class="jFiler-item-thumb-image"><img src="../img/curso/<?php echo $objPrograma->__get('_NOM_IMAGEN'); ?>" draggable="false"></div>                                
            </div>                                
            <div class="jFiler-item-assets jFiler-row">                                    
            <ul class="list-inline pull-left">                                        
            <li><div class="jFiler-jProgressBar" style="display: none;"><div class="bar" style="width: 100%;">
            </div></div><div class="jFiler-item-others text-success"><i class="icon-jfi-check-circle"></i> Correcto</div></li>                                    
            </ul>                                    
            <ul class="list-inline pull-right">                                        
            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>                                    
            </ul>                                
            </div>                            
            </div>                        
            </div>                    
            </li></ul></div>


            <!-- <div class="form-group col-sm-12">
         <center>
          <label>Imagen Slider (1140x350px)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input3" multiple>
                                </div>
                                </center>
                      </div>-->


            
           <!-- <div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid">
            <li class="jFiler-item" data-jfiler-index="0" style="">                        
            <div class="jFiler-item-container">                            
            <div class="jFiler-item-inner">                                
            <div class="jFiler-item-thumb">                                    
            <div class="jFiler-item-status"></div>                                    
            <div class="jFiler-item-info">                                        
           <span class="jFiler-item-title"><b title="<?php// echo $objPrograma->__get('_NOM_IMAGEN_SLIDER'); ?>"><?php// echo $objPrograma->__get('_NOM_IMAGEN_SLIDER'); ?></b></span> 
            <span class="jFiler-item-others">KB</span>                                 
            </div>                                    
            <div class="jFiler-item-thumb-image"><img src="../img/curso/<?php //echo $objPrograma->__get('_NOM_IMAGEN_SLIDER'); ?>" draggable="false"></div>                                
            </div>                                
            <div class="jFiler-item-assets jFiler-row">                                    
            <ul class="list-inline pull-left">                                        
            <li><div class="jFiler-jProgressBar" style="display: none;"><div class="bar" style="width: 100%;">
            </div></div><div class="jFiler-item-others text-success"><i class="icon-jfi-check-circle"></i> Correcto</div></li>                                    
            </ul>                                    
            <ul class="list-inline pull-right">                                        
            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>                                    
            </ul>                                
            </div>                            
            </div>                        
            </div>                    
            </li></ul></div>-->
    

           <!-- <div class="form-group col-sm-12">
         <center>
          <label>Imagen Expositor (800x800px)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input4" multiple>
                                </div>
                                </center>
                      </div>-->


            
           <!-- <div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid">
            <li class="jFiler-item" data-jfiler-index="0" style="">                        
            <div class="jFiler-item-container">                            
            <div class="jFiler-item-inner">                                
            <div class="jFiler-item-thumb">                                    
            <div class="jFiler-item-status"></div>                                    
            <div class="jFiler-item-info">                                        
           <span class="jFiler-item-title"><b title="<?php //echo $objPrograma->__get('_NOM_IMAGEN_EXPOSITOR'); ?>"><?php //echo $objPrograma->__get('_NOM_IMAGEN_EXPOSITOR'); ?></b></span> 
            <span class="jFiler-item-others">KB</span>                                 
            </div>                                    
            <div class="jFiler-item-thumb-image"><img src="../img/curso/<?php //echo $objPrograma->__get('_NOM_IMAGEN_EXPOSITOR'); ?>" draggable="false"></div>                                
            </div>                                
            <div class="jFiler-item-assets jFiler-row">                                    
            <ul class="list-inline pull-left">                                        
            <li><div class="jFiler-jProgressBar" style="display: none;"><div class="bar" style="width: 100%;">
            </div></div><div class="jFiler-item-others text-success"><i class="icon-jfi-check-circle"></i> Correcto</div></li>                                    
            </ul>                                    
            <ul class="list-inline pull-right">                                        
            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>                                    
            </ul>                                
            </div>                            
            </div>                        
            </div>                    
            </li></ul></div>-->


                      
      </div>
      </div>

</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   