 <div class="topbar clearfix">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-9 col-sm-9 text-left">
                            <p>
                        <strong><i class="fa fa-phone" style="color:white"></i></strong>01-614-7800 anexo 130 -522 &nbsp;&nbsp; <strong><i class="fa fa-envelope" style="color:white"></i></strong> <a >capacitacionfape@lamolina.edu.pe - ueups_fape@lamolina.edu.pe</a>
                            </p>
                        </div><!-- end left -->
                        <div class="col-md-3 col-sm-3  text-right">
                            <div class="social">
                            <center>
                             <a class="facebook" href="https://www.facebook.com/FAPE.UEUPS.UNALM/" data-tooltip="tooltip" data-placement="bottom" title="Visita Nuestro Facebook" target="_blank">
                                <i class="fa fa-facebook" style="color:#155c8f"></i>
                                </a>              
                <a class="facebook" href="https://api.whatsapp.com/send?phone=51977951385" data-tooltip="tooltip" data-placement="bottom" title="Visita Nuestro Facebook" target="_blank">
                                <i class="fa fa-whatsapp" style="color:#155c8f"></i>
                                </a>      
                            </center>
                        
                            </div><!--end social -->
                        </div><!-- end left -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end topbar -->
            <div class="container-fluid">
                <nav class="navbar navbar-default yamm">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo-normal">
                            <a class="navbar-brand" href="index.php"><img src="images/ueups.png" alt=""></a>
                        </div>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Inicio</a></li>
                            <li><a href="index.php#extension">Ex. Universitaria</a></li>
                            <li><a href="index.php#proy_social">Pro. Social</a></li>
                            <li><a data-toggle="modal" data-target="#myModal" style="cursor:pointer">Prácticas</a></li>
                            <li><a href="#">Movilidad</a></li>
                            <li><a href="http://www.lamolina.edu.pe/bienestar/biene.htm">Bienestar</a></li>
                            <li><a href="galeria.php">Galería</a></li>
                            <li><a href="../campus_social" target="_blank">Campus Virtual</a></li>
                            
                          <!-- <li class="dropdown hassubmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Shop <span class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="shop.html">Shop Layout</a></li>
                                    <li><a href="shop-single.html">Shop Single</a></li>
                                </ul>
                            </li>-->
                            <!--<li class="dropdown hassubmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Blog <span class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="blog.html">Blog Right Sidebar</a></li>
                                    <li><a href="blog-1.html">Blog Left Sidebar</a></li>
                                    <li><a href="blog-2.html">Blog Grid Sidebar</a></li>
                                    <li><a href="blog-3.html">Blog Grid Fullwidth</a></li>
                                    <li><a href="blog-single.html">Blog Single</a></li>
                                </ul>
                            </li>-->
                        </ul>
                    </div>
                </nav><!-- end navbar -->
            </div><!-- end container -->
            