<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = '';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_ListarRedes($conexion);

	include $nombreModulo . 'vistas/listar.php';

}


function loadURL($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelPrograma 	= new Data_sgaprograma();
	$objPrograma 	= $modelPrograma->fu_EncontrarRedSocial($conexion,$id);
	$id				= $id;

	include $nombreModulo . 'vistas/editar.php';

}

function editarUrl($id,$url){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	if(trim($url) == ''){ $url = '#';}

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_guardarUrl($conexion,$id,$url,$idUsuario); 

	if($rpta>=1){echo 'passed';}
	else{ echo 'failed';}
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar();
		 break;	
	case 'loadURL':
		loadURL($_POST['id']);break;	
	case 'editarUrl':
		editarUrl($_POST['idUrl'],$_POST['url']);break;	
    default:
        vistaPrincipal();
        break;
}
?>