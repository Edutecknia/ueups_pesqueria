<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2015 © 
* @package       - - -
* @name          class.sgaimagen.php
* [Descripcion] :: Clase imagen
 * */
class Data_sgaimagen{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

//Encontrar imagen	
public function fu_listar($conexion,$idSociedad,$tipoImagenSitio,$indActivo,$pagina) {
		try {
			$paginado = TAM_PAG_LISTADO;
			
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($tipoImagenSitio)==""){$tipoImagenSitio='NULL';}else{ $tipoImagenSitio= "'" . trim($tipoImagenSitio) . "'"  ;}
						
			$sql 	= "CALL USP_IMAGEN_SITIO_LISTA_GESTION($idSociedad, $tipoImagenSitio,$indActivo,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_registrar($conexion,$orden,$idUsuario,$idSociedad) {
		try {
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}

			$sql = "CALL USP_IMAGEN_SITIO_ACTUALIZAR_ORDEN_IMAGEN_SITIO($idSociedad, $orden, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_inactivar($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_IMAGEN_SITIO_INACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_activar($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_IMAGEN_SITIO_ACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_Encontrar($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT NOMBRE_IMAGEN_SITIO FROM IMAGEN_SITIO WHERE ID_IMAGEN_SITIO = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["NOMBRE_IMAGEN_SITIO"] != ""):
					$obj_sgaimagen = new Data_sgaimagen();
					$obj_sgaimagen->__set("_NOMBRE_IMAGEN_SITIO", $row["NOMBRE_IMAGEN_SITIO"]);
				endif;
		
			endforeach;
		return $obj_sgaimagen;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}			
	
}
?>