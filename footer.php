            
            <div class="container-fluid">
             <div class="row">
              <div class="col-md-3">
             <div class="widget clearfix">  
             <div class="row">
        <div class="col-md-12 text-center section-heading probootstrap-animate">
              <h3 class="mb0" style="color: #fff;text-align: left">Contacto</h3>
            </div> 
          </div>  
             <div class="row">
                 <div class="col-md-12">
                     <ul style="list-style: none;padding: 0">
                         <li style="padding:0px 0px 13px 0px"><a href="https://web.facebook.com/pesqueria.lamolina.edu.pe" target="_blank"><span><i class="icon-facebook2"></i> Visita Nuestro Facebook</span></a></li>
                         <li style="padding:0px 0px 13px 0px"><a ><span><i class="icon-phone2"></i> 614 7800 anexo 207</span></a></li>
                         <li style="padding:0px 0px 13px 0px"><a ><span><i class="icon-print"></i> 614 7143 anexo 643</span></a></li>
                         <li style="padding:0px 0px 13px 0px"><a><span><i class="icon-mail22"></i> fpesqueria@lamolina.edu.pe</span></a></li>
                         <li style="padding:0px 0px 13px 0px"><a href="https://www.google.com/maps?ll=-12.083943,-76.944466&z=16&t=m&hl=es-ES&gl=PE&mapclient=embed&cid=17977433989204872140"><span><i class="icon-map22"></i> AV. La Molina s/n La Molina - <br>Lima - Lima - Perú <br>Ubícanos en el Mapa</span></a></li>
                     </ul>
                 </div>
             </div>
                  </div> 
                      
              </div>
              <div class="col-md-3">
                     <div class="widget clearfix">
                     <div class="row">
                         <div class="col-md-12 text-center section-heading probootstrap-animate">
              <h3 class="mb0" style="color: #fff;text-align: center">Enlaces de Interés</h3>
            </div>
                     </div> 
                     <div class="row">
                       <div class="col-md-12">
                        <ul style="list-style: none">
                            <li style="padding:0px 0px 13px 0px"><a href="acreditacion.php" >Acreditación</a></li>
                <li style="padding:0px 0px 13px 0px"><a href="http://tumi.lamolina.edu.pe/cendoc/pesqueria/jbusqueda.htm" target="_blank">Biblioteca Virtual FaPe</a></li>
        <li style="padding:0px 0px 13px 0px"><a  data-toggle="modal" data-target="#buzon" style="cursor:pointer">Buzón de Quejas y Sugerencias</a></li>
                            <li style="padding:0px 0px 13px 0px"><a href="">Transparencia</a></li>
                            <li style="padding:0px 0px 13px 0px"><a href="">Publicaciones</a></li>
                         <li style="padding:0px 0px 13px 0px"><a data-toggle="modal" data-target="#entidades" style="cursor:pointer">Entidades e Instituciones del Sector</a></li>

                        </ul> 
                         </div>
                     </div>
                  </div>
                     
              </div>
              
              <div class="col-md-3">
                     <div class="widget clearfix">
                     <div class="row">
                         <div class="col-md-12 text-center section-heading probootstrap-animate">
              <h3 class="mb0" style="color: #fff;text-align: center">UNALM</h3>
            </div>
                     </div>
                     <div class="row">
                      <div class="col-md-12">
                          <ul style="list-style: none">
                              <li><a href="https://admision.lamolina.edu.pe/" target="_blank">Admisión</a></li>
                              <li><a href="">Becas y Convenios Institucionales</a></li>
                              <li><a href="http://tumi.lamolina.edu.pe/ban/" target="_blank">Biblioteca Agrícola Nacional</a></li>
                              <li><a href="">Bolsa De Trabajo</a></li>
                              <li><a href="http://www.lamolina.edu.pe/idiomas/html/principal.htm" target="_blank">Centro de Idiomas Unalm</a></li>
                       <li><a href="http://www.preuniversidadagraria.edu.pe/index.php?pagina=inicio&menu=inicio" target="_blank">Centro de Estudios Preuniversitarios UNALM (CEPRE-UNALM)</a></li>
                              <li><a href="http://www.lamolina.edu.pe/portada/html/acerca/intranet_total.html">Intranet UNALM</a></li>
                          </ul>
                      </div>   
                     </div>
                  </div>
              </div>
                 <div class="col-lg-3 col-md-3">
                        <div class="widget clearfix">
                        <center>
                        <br>
                        <br>
                        <br>
                         <img src="images/escudo2.png" width="55%" alt="" class="img-responsive">   
                        </center>
                        </div><!-- end widget -->
                    </div><!-- end col -->

          </div>
            
               
               
               
               
               
               
               
               
               
    
  <?php 
    
    include('modal.php');
    
    ?>