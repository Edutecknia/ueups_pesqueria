<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = '';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($campo,$orden,$direccion,$pagina,$estado,$filtro){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_Listar($conexion,$idSociedad, $filtro, '', 2, $estado,$pagina);

	foreach ($arrayPrograma as $obj):
        $numTotalElementos = $obj['FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';
	//orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		//campo: campo
	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var orden = $("#orden").val();
							var direccion = $("#direccion").val();
							var valor = $("#txtBuscar").val();
							var campo = "";
							var estado=$("#cboEstado").val();
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/online/online.php?cmd=listar", {
						orden:orden, direccion:direccion, valor:valor,pagina:num,estado:estado,campo:campo
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	include $nombreModulo . 'vistas/crear.php';
}

function registrar($nombre, $orden, $slider){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_registrarOnline($conexion,$idSociedad, $nombre, $orden, $slider, $idUsuario); 

	if($rpta=='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$obj_sgaprograma = new Data_sgaprograma();
	$obj_sgaprograma = $obj_sgaprograma->fu_Encontrar($conexion,$id);
	$id = $id;
	include $nombreModulo . 'vistas/editar.php';

}

function loadVisualizar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$obj_sgaprograma = new Data_sgaprograma();
	$obj_sgaprograma = $obj_sgaprograma->fu_Encontrar($conexion,$id);
	$id = $id;
	$indVisualizar = 1;
	include $nombreModulo . 'vistas/editar.php';

}

function editar($id,$nombre, $orden, $slider){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_editarOnline($conexion,$id, $idSociedad, $nombre, $orden, $slider, $idUsuario); 

	if($rpta=='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_inactivar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_activar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}

function loadCreacionCurso($idPrograma){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$obj_sghtipohabitacion = new Data_sghtipohabitacion();
	$obj_sghtipohabitacion =$obj_sghtipohabitacion->fu_offset($conexion,$id);
	$idPrograma = $idPrograma;
	include $nombreModulo . 'vistas/listar_curso.php';

}

function eliminar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_eliminar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['campo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['estado'],$_POST['valor']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['nombre'],$_POST['orden'],$_POST['slider']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['id'],$_POST['nombre'],$_POST['orden'],$_POST['slider']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'loadCreacionCurso':
		loadCreacionCurso($_POST['id']);break;	
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;
	case 'eliminar':
		eliminar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>