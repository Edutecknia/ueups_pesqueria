<form action="javascript:registrarUsuario();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">



  <div class="form-group col-sm-6">
                <label>Nombres:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreUsuario" name="nombreUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre del usuario" 
                                    type="text" 
                                    maxlength="50" 
                                    required="required">
                                </div>
                        </div>

  <div class="form-group col-sm-6">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apellidoUsuario" name="apellidoUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese apellidos del usuario" 
                                    type="text" 
                                    maxlength="50" 
                                    required="required">
                                </div>
                        </div>                        


<div class="form-group col-sm-6">
                <label>Tipo Documento:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <select class="form-control" name="cboTipoDocumento" id="cboTipoDocumento">
                                <?php foreach ($arrayTipoDocumento as $obj): ?>
                                <option value="<?php echo $obj['ID_DOCUMENTO_IDENTIDAD']; ?>"><?php echo $obj['ABR_DOCUMENTO_IDENTIDAD']; ?></option>
                                <?php endforeach; ?>
                                </select>
                                </div>
                        </div>  

<div class="form-group  col-sm-6">
                <label>Número Documento:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="numDocumento" name="numDocumento" 
                                    class="form-control" 
                                    placeholder="Ingrese número de documento de identidad" 
                                    type="text" 
                                    maxlength="10" 
                                    required="required">
                                </div>
                        </div>
                                             


  <div class="form-group col-sm-6">
                <label>Usuario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="loginUsuario" name="loginUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese login de usuario" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div> 

<div class="form-group col-sm-6">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailUsuario" name="mailUsuario" 
                                    class="form-control" 
                                    placeholder="E-mail del usuario" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                        </div> 


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   