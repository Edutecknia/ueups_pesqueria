﻿          
            <input type="hidden" id="txtSuma" value="<?php echo $suma ?>">
            <input type="hidden" id="txtTotal" value="<?php echo $total ?>">

            <?php if(count($arrayProgramma) > 0){ ?>

                <?php
                $contador = 0;
        		
                foreach ($arrayProgramma as $obj):
                $contador++;

                $fecha      = '';
                $enlace     = '#';

                if($obj['TIPO'] == 1){

                    $fecha  = $obj['NOM_CIUDAD'];
                    
                    $enlace = 'detalle.php?cmd='.$obj['ID_PROGRAMA'];
                }
       			?>
                <div class="col-md-3 col-sm-3 wow slideInLeft">
                <div class="course-box">
            <?php 

            $source = $obj['NOM_IMAGEN'];
            $ruta   = 'curso';

            if($obj['TIPO'] == '1'){

              $ruta = 'actualizacion_presencial';
            } 
            ?>


                            <div class="image-wrap entry">
                                <img src="img/<?php echo $ruta; ?>/<?php echo $source; ?>" alt="" class="img-responsive">
                                <div class="magnifier">
                                     <a href="<?php echo $enlace; ?>" title=""><i class="fa fa-eye"></i></a>
                                </div>
                            </div><!-- end image-wrap -->
                            <div class="course-details">
                                <h4>
                                    <a href="#" title=""><?php echo $obj['NOM_PROGRAMA']; ?></a>
                                </h4>
                                <div class="hidden-md">
                                      <p>&nbsp;</p>
                                </div>
                            </div><!-- end details -->
                            <div class="course-footer clearfix">
                                <div class="pull-left">
                                    <ul class="list-inline">
                                        <li><a><i class="fa fa-calendar"></i> <?php echo $fecha; ?></a></li>
                                    </ul>
                                </div><!-- end left -->
                            </div><!-- end footer -->
       			
                
                </div>
                </div>

                <?php
                if($contador ==4){
                    $contador = 0;
                    echo '<div style="clear:both;"></div>';
                }
      			endforeach;
      			?>
                    
                    <div style="clear:both;"></div>
                   
            <?php } ?>    
            <!--CAROUSEL BOOTSTRAP-->