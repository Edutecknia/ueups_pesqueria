<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
            
            $modelData              = new Data_sgaprograma();
            $arrayGaleria           = $modelData->fu_listarGaleria($conexion);
            $arrayGaleriaImg        = $modelData->fu_listarGaleriaImg($conexion);
?>
<!doctype html>
 <html class="no-js " lang="es">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Unidad de Extensión y Proyeccion Social</title>
    <!-- Site Icons -->
    <link rel="shortcut icon" href="../web_pesqueria/img/escudo2.png" type="image/x-icon" />
	<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet"> 
	<link href="http://allfont.es/allfont.css?fonts=franklin-gothic-heavy" rel="stylesheet" type="text/css" />
    <!-- Custom & Default Styles -->
	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="style.css">


</head>
<body>  


    <!-- END LOADER -->
    <div id="wrapper">
        
       
        <!-- END # MODAL LOGIN -->
         <header class="header">
           
        <?php include('nav.php') ?>
        </header>
        <section class="" id="fondo1">
            <div class="container">
               <br>
               <br>
               <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="tagline-message page-title">
                            <h3>Galería de Fotos</h3>
                        </div>
                    </div><!-- end col -->
                    <div class="col-md-6 text-right">
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
 

<section class="section">
 <div class="container">
    <?php 
    foreach ($arrayGaleria as $objGaleria) {
    $activo = 'active';
    ?>
     <br>
     <div class="row">
           <br>
       <div class="section-title text-center">
             <h3 style="color:#155c8f">><?php echo $objGaleria['NOM_GALERIA'];?></h3>
         </div><!-- end title -->
            <div class="span12">
                <div class="well">
                    <div id="myCarousel" class="carousel fdi-Carousel slide">
                     <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                            <?php foreach ($arrayGaleriaImg as $objImg) {
                             if($objImg['ID_GALERIA'] == $objGaleria['ID_GALERIA']){
                                ?>
                                <div class="item active">
                                    <div class="col-md-3">
                                <img src="archivos/ueups/<?php echo $objImg['NOM_IMG'];?>" alt="" class="img-responsive" center-block>
                                    </div>
    
                                </div>
                               <?php
                                }
                            }
                                ?>    
                        
                            </div>
                            <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                        </div>
                        <!--/carousel-inner-->
                    </div><!--/myCarousel-->
                </div><!--/well-->
            </div>
        </div>
       <hr> 
         <?php
    }
    ?>

      
 </div>

        </section>



        <footer class="section footer noover">
       <?php include('footer.php')?>
        </footer>
    </div><!-- end wrapper -->

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/custom.js"></script>
    <script>
    
    
    </script>
    <!-- VIDEO BG PLUGINS -->

</body>
</html>