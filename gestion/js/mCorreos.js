// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
     loadMenu('correos');rutaMenu('Correos de destino','Front Desk','Correos');
}

function listado(){
	INI_LOAD();
    $.post("modulos/correos/correos.php?cmd=listar", {
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}



function OpenForm(id)
{
    var cmd 		= "loadCorreo";
    var titulo 		= "";
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/correos/correos.php?cmd=" + cmd, {id: id},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });
}

function editarCorreo(){
	var idCorreo 		= $("#txtid").val();
	var correo 	   	    = $("#txtcorreo").val();

		INI_LOAD();
    $.post("modulos/correos/correos.php?cmd=editarCorreo", {
        idCorreo:idCorreo,correo:correo
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			listado();			
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
			   QuitarModal();
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}


