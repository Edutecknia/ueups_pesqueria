 <!-- Modal -->
 <div id="modal_inscripcion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="section-title text-center">
            <h2 style="color:#155c8f;font-weight:bold">COMPLETA LA FICHA DE INSCRIPCIÓN</h2>
            <hr>
           </div>
      </div>
      <div class="modal-body">
        <div class="container">
                 <div class="row">
                   <div class="row">
                      <div class="col-md-1"></div>
                       <div class="col-md-10">
                         <p>&nbsp;&nbsp;<b style="color:red">IMPORTANTE:</b> Antes de llenar la ficha de Inscripción se deberá de realizar el pago correspondiente en la cuenta bancaria indicada.</p>  
                       </div>
                       <div class="col-md-1"></div>
                   </div>
                    <div class="col-md-1"></div>
                     <div class="col-md-10">
                    <form action="../contacto/charla.php" method="post" class="big-contact-form">
                        <div class="section-title" style="margin:0">
                        <h4 style="color:#155c8f;font-weight:bold">Datos Personales</h4>
                        <hr style="margin: 10px 0px;">
                       </div>  
                          <ul style="list-style:none;padding:0;margin:0">
                     <li>
                    <span><label for="">Apellidos:</label><input type="text" name="txt_apellidos" required class="form-control" placeholder="Ingrese apellidos"></span>
                     </li>
                    <li>
                     <span><label for="">Nombre:</label><input type="text" name="txt_nom" required class="form-control" placeholder="Ingrese nombre"></span>
                    </li>
                 <li style="display:inline-block;width:49%"><span><label for=""  style="text-align:left">Fecha de Nac:</label><input type="date" class="form-control" required name="fecha_nac"></span></li>
                       
                    <li  style="display:inline-block;width:49%">
                    <span><label for="" style="text-align:left">Sexo:</label>
                     <select name="slt_genero" required id="" class="form-control">
                     <option >Seleccione</option>
                     <option value="masculino">Masculino</option>
                     <option value="femenino">Femenino</option>
                     </select>
                     </span>
                     </li>  
                        <li>
                        <span><label for="">DNI:</label><input type="text" required name="txt_dni" class="form-control" placeholder="Ingrese DNI"></span>
                        </li> 
                        <li>
                    <span><label for="">Grado de instrucción:</label><input type="text" name="txt_grado_inst" required class="form-control" placeholder="Ingrese grado"></span>
                        </li> 
                        <li>
                        <span><label for="">Correo electrónico:</label><input  name="txt_correo" required type="text" class="form-control" placeholder="Ingrese correo electronico"></span>
                        </li>  
                        <li style="display:inline-block;width:49%"><span><label for=""  style="text-align:left">Teléfono fijo y movil:</label><input type="text" name="txt_telefono1" placeholder="telefono 1" required class="form-control"></span>
                        </li>
                        <li  style="display:inline-block;width:49%">
                        <span><input type="text" required name="txt_telefono2" placeholder="telefono 2" class="form-control"></span>
                        </li> 
                        <li >
                        <span>
                        <label for="">Dirección:</label>
                        <input type="text" name="txt_direccion1" placeholder="Ingrese direccion" required class="form-control">
                        </span>
                        </li> 
                        
                    <li style="display:inline-block;width:49%"><span>
                    <label for=""  style="text-align:left">Distrito:</label>
                    <center><input type="text" required name="txt_distrito" placeholder="ingrese distrito" class="form-control"> </center>
                   </span>
                    </li>
                    <li  style="display:inline-block;width:49%">
                        <span><label for=""  style="text-align:left">Ciudad:</label>
                        <center><input type="text" name="txt_ciudad" placeholder="Ingrese ciudad" required class="form-control"></center></span>
                    </li>
                      <li style="display:inline-block;width:49%"><span>
                    <label for=""  style="text-align:left">Provincia:</label>
                    <center><input type="text" name="txt_provincia" placeholder="Ingrese provincia" required class="form-control"> </center>
                   </span>
                    </li>
                    <li  style="display:inline-block;width:49%">
                        <span><label for=""  style="text-align:left">País:</label>
                        <center><input type="text" required name="txt_pais" placeholder="Ingrese pais" class="form-control"></center></span>
                    </li> 
                   <div class="section-title" style="margin:0">
                        <h4 style="color:#155c8f;font-weight:bold">Datos laborales y/o estudios</h4>
                        <hr style="margin: 10px 0px;">
                    </div>  
                     <li>
                        <span>
                        <label for="">Centro de Trabajo o estudios:</label>
                        <input type="text" name="txt_centro" placeholder="Ingrese centro de trabjo y/o estudio" required class="form-control">
                        </span>
                    </li>
                    
                    <li style="display:inline-block;width:49%"><span>
                    <label for=""  style="text-align:left">Cargo Actual:</label>
                    <center><input type="text" name="txt_cargo" placeholder="Ingrese cargo actual" required class="form-control"> </center>
                   </span>
                    </li>
                    <li style="display:inline-block;width:49%"><span>
                    <label for=""  style="text-align:left">Teléfono:</label>
                    <center><input type="text" name="txt_telefono_centro" placeholder="Ingrese telefono" class="form-control"> </center>
                   </span>
                    </li> 
                    <div class="section-title" style="margin:0">
                        <h4 style="color:#155c8f;font-weight:bold">Facturación: Seleccione el comprobante que desea recibir</h4>
                        <hr style="margin: 10px 0px;">
                    </div>  
                    <li>
                    <span><label for="" style="text-align:left">Tipo de Comprobante:</label>
                     <select name="slt_comprobante" required class="form-control"  onchange="if(this.value=='factura') {    
                        document.getElementById('f1').disabled = false
                        document.getElementById('f2').disabled = false
                        document.getElementById('f3').disabled = false
                        } else {
                        document.getElementById('f1').disabled = true
                        document.getElementById('f2').disabled = true
                          document.getElementById('f3').disabled = true  
                         }">
                     <option value=""></option>
                     <option>Seleccione</option>
                     <option value="boleta">Boleta</option>
                     <option value="factura">Factura</option>
                        </select></span>
                    </li> 
                    <li  style="display:inline-block;width:49%">
                        <span><label for=""  style="text-align:left">Razón Social:</label>
                        <center><input id="f1" name="txt_razon" type="text" placeholder="Ingrese Razon social" class="form-control" disabled="disabled"></center></span>
                    </li>
                    <li style="display:inline-block;width:49%"><span>
                    <label for=""  style="text-align:left">RUC:</label>
                    <center><input id="f2" name="txt_ruc" type="text" placeholder="Ingrese RUC"class="form-control"disabled="disabled"> </center>
                   </span>
                    </li> 
                   <li ><span>
                    <label for=""  style="text-align:left">Dirección:</label>
                    <center><input id="f3" name="txt_direccion" placeholder="Ingrese direccion" type="text" class="form-control " disabled="disabled"> </center>
                   </span>
                    </li>
                    <div class="section-title" style="margin:0">
                        <h4 style="color:#155c8f;font-weight:bold">Datos de voucher</h4>
                        <hr style="margin: 10px 0px;">
                    </div> 
                    <li style="display:inline-block;width:32%"><span>
                    <label for=""  style="text-align:left">N° de Operación:</label>
                    <center><input  type="text" class="form-control" placeholder="Ingreso N° de operacion" required name="txt_nro_operacion"> </center>
                   </span>
                    </li>     
                    <li style="display:inline-block;width:32%"><span>
                    <label for=""  style="text-align:left">Monto:</label>
                    <center><input type="text" class="form-control" placeholder="Ingrese monto" required name="txt_monto"> </center>
                   </span>
                    </li> 
                    <li style="display:inline-block;width:32%"><span>
                    <label for=""  style="text-align:left">Fecha de Operación:</label>
                    <center><input  type="date" class="form-control" required name="dt_fecha_operacion"> </center>
                   </span>
                    </li> 
                    <li><span>
                    <label for=""  style="text-align:left">En caso no se complete el número mínimo de participantes:</label>
                    <center>
                        <select name="slt_requerimiento" required class="form-control" id="">
                            <option >Seleccione</option>
                            <option value="reprogramar">Reprogramar</option>
                            <option value="reembolso">Solicitar Reembolso</option>
                            <option value="contactarme">Comunicarse al teléfono de contacto</option>
                        </select>
                    </center>
                   </span>
                    </li>    
                    <div class="section-title" style="margin:0">
                        <h4 style="color:#155c8f;font-weight:bold">Información adicional</h4>
                        <hr style="margin: 10px 0px;">
                    </div>  
                    
                    <li style="display:inline-block;width:40%">
                    <span>
                    <label for=""  style="text-align:left">¿Como se entero de la charla?</label>
             <select name="txt_medios" required class="form-control"  onchange="if(this.value=='otros') {    document.getElementById('otros').disabled = false } else {document.getElementById('otros').disabled = true }" >
                      <option >Seleccione</option>
                       <option value="Periódico">Periódico</option>
                       <option value="Volante">Volante</option>
                       <option value="Afiche">Afiche</option>
                       <option value="web">Página web</option>
                       <option value="Ejecutivo">Ejecutivo de ventas</option>
                       <option value="Facebook">Facebook</option>
                       <option value="LinkedIn">LinkedIn</option>
                       <option value="Twitter">Twitter</option>
                       <option value="Google">Google</option>
                       <option value="Amigos">Amigos</option>
                       <option value="otros">Otros</option>
                   </select>  
                   </span>
                    </li> 
                    <li style="display:inline-block;width:59%"><center>
                    <input id="otros" name="txt_otros" type="text" placeholder="otros" class="form-control" disabled></center></li>    
                    <li >
                    <span>
                    <label for=""  style="text-align:left">¿Qué otra charla le gustaria llevar?</label>
                    <center>
                    <input  type="text" class="form-control" placeholder="Ingrese su comentario" required name="txt_capacitacion_adicional"> 
                    </center>
                   </span>
                    </li>  
                    <input type="hidden" value="<?php echo $nomcurso; ?>" name="txt_nom_curso"> 
                    <input type="hidden" value=" <?php echo $horario; ?>" name="txt_inicio_clases"> 
                    </ul>
                    <br>  
                     <center>
                        <button type="submit" class="btn btn-default">Enviar Ficha</button>
                     </center>
                    </form>
                    
                      </div>
                      <div class="col-md-1"></div>
                 </div>   
                
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
   