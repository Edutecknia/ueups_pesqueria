<?php
class Data_Carrito
{
 
    //aquí guardamos el contenido del carrito
    private $carrito = array();
 
    //seteamos el carrito exista o no exista en el constructor
    public function __construct()
    {
        
        if(!isset($_SESSION["carrito"]))
        {
            $_SESSION["carrito"] = null;
            $this->carrito["precio_total"] = 0;
            $this->carrito["articulos_total"] = 0;
        }
        $this->carrito = $_SESSION['carrito'];
    }
 
    //añadimos un producto al carrito
    public function add($articulo = array())
    {
        //primero comprobamos el articulo a añadir, si está vacío o no es un 
        //array lanzamos una excepción y cortamos la ejecución
        if(!is_array($articulo) || empty($articulo))
        {
            throw new Exception("Error, el articulo no es un array!", 1);    
        }
 
        //nuestro carro necesita siempre un id producto, cantidad y precio articulo || !$articulo["precio"]
        if(!$articulo["id"] || !$articulo["cantidad"])
        {
            throw new Exception("Error, el articulo debe tener un id, cantidad y precio!", 1);    
        }
 
        //nuestro carro necesita siempre un id producto, cantidad y precio articulo
        if(!is_numeric($articulo["id"]) || !is_numeric($articulo["cantidad"]) || !is_numeric($articulo["precio"]))
        {
            throw new Exception("Error, el id, cantidad y precio deben ser números!", 1);    
        }
 
        //debemos crear un identificador único para cada producto
        //$unique_id = $articulo["id"];//md5($articulo["id"]);
 
        //creamos la id única para el producto
        //$articulo["unique_id"] = $unique_id;
        
        //si no está vacío el carrito lo recorremos 
        if(!empty($this->carrito))
        {
			$nid = 0;
            foreach ($this->carrito as $row) 
            {
                //comprobamos si este producto ya estaba en el 
                //carrito para actualizar el producto o insertar
                //un nuevo producto    
				
				if($row["unique_id"] !=""){
					$nid = $nid += 1;
				}  

                //if($row["unique_id"] === $unique_id)
                //{
					
                    //si ya estaba sumamos la cantidad
                    //$articulo["cantidad"] = $row["cantidad"] + $articulo["cantidad"];
                //}
            }
			 $nid = $nid += 1;
			 //verificar si id no existen en carrito
			 $flag = 1;
			 while ($flag = 1) {
			 
			 $flag = $this->verificar($nid);
			 if($flag==1){ //si id generado aumentar en 1 y volver a verificar
				$nid = $nid += 1;	
			 }
			 else{ break;}
			 
			 }
			 
			 $unique_id = $nid; 
			 $articulo["unique_id"] = $unique_id;
        }
		else{
			 $unique_id = $nid += 1;
			 $articulo["unique_id"] = $unique_id;
		}
 
        //evitamos que nos pongan números negativos y que sólo sean números para cantidad y precio
        $articulo["cantidad"] = trim(preg_replace('/([^0-9\.])/i', '', $articulo["cantidad"]));
        $articulo["precio"] = trim(preg_replace('/([^0-9\.])/i', '', $articulo["precio"]));
 		
        //añadimos un elemento total al array carrito para 
        //saber el precio total de la suma de este artículo
		
		$articulo["total"] = $articulo["subtotal"];
		//original
		//$articulo["total"] = $articulo["cantidad"] * $articulo["precio"];
		//$dscto = $articulo["descuento"] / 100;
		//$articulo["valordscto"] = ($articulo["total"] * $dscto);
		//$articulo["total"] = number_format($articulo["total"] - $articulo["valordscto"], 2, '.', ''); 
		//fin original
		
		$dscto = $articulo["descuento"];  // / 100;
		$articulo["valordscto"] = 0;
		if($articulo["precioIGV"]=='MODIF'){
		 $dscto = 0;//($articulo["total"] * $dscto);
		}
		$articulo["valordscto"] = $dscto;
		
		$articulo["total"] = $articulo["total"] - $articulo["valordscto"];
		$articulo["total"] = number_format($articulo["total"], 2, '.', ''); 
		$articulo["igv"] = number_format($articulo["Montoigv"], 2, '.', ''); //$articulo["Montoigv"];
		
		if($articulo["precioIGV"]=='MODIF'){
		 $articulo["valordscto"] = $articulo["descuento"];
		}
		
		
        //primero debemos eliminar el producto si es que estaba en el carrito
        //$this->unset_producto($unique_id);
 		
        ///ahora añadimos el producto al carrito
        $_SESSION["carrito"][$unique_id] = $articulo;
 
        //actualizamos el carrito
        $this->update_carrito();
 
        //actualizamos el precio total y el número de artículos del carrito
        //una vez hemos añadido el producto
        $this->update_precio_cantidad();
 
    }
 
 public function verificar($nid){
	
	$flag = 0;
	foreach ($this->carrito as $row) 
    {
			 if($row["unique_id"] === $nid)
			 {
			  $flag = 1;
			  break;	   
			 }			  
	}
	return $flag;
	/*while ( $cuenta <= 10) {
		echo "Cuenta vale $cuenta <br>";
		$cuenta++;
	}*/
		
}
    //método que actualiza el precio total y la cantidad
    //de productos total del carrito
    private function update_precio_cantidad()
    {
        //seteamos las variables precio y artículos a 0
        $precio = 0;
        $articulos = 0;
		$descuento = 0;
		$valordcto = 0;
		$igv = 0;
 
        //recorrecmos el contenido del carrito para actualizar
        //el precio total y el número de artículos
        foreach ($this->carrito as $row) 
        {
            $precio +=  $row['subtotal'];//($row['precio'] * $row['cantidad']);
            $articulos += $row['cantidad'];
			$descuento += $row['descuento'];
			$valordcto += $row["valordscto"];
			$igv += number_format($row["igv"], 2, '.', ''); //$row["igv"];
        }
        //asignamos a articulos_total el número de artículos actual
        //y al precio el precio actual
        $_SESSION['carrito']["articulos_total"] = $articulos;
        $_SESSION['carrito']["precio_total"] = $precio;
		$_SESSION['carrito']["total_dscto"] = $descuento;
		$_SESSION['carrito']["valordscto"] = $valordcto;
		$_SESSION['carrito']["igv"] = $igv;
		
 
        //refrescamos él contenido del carrito para que quedé actualizado
        $this->update_carrito();
    }
 	//metodo que devuelve el monto total del carrito sin igv
	 
	
	
    //método que retorna el precio total del carrito
    public function precio_total()
    {
        //si no está definido el elemento precio_total o no existe el carrito
        //el precio total será 0
        if(!isset($this->carrito["precio_total"]) || $this->carrito === null)
        {
            return 0;
        }
        //si no es númerico lanzamos una excepción porque no es correcto
        if(!is_numeric($this->carrito["precio_total"]))
        {
            throw new Exception("El precio total del carrito debe ser un número", 1);    
        }
        //en otro caso devolvemos el precio total del carrito
        return $this->carrito["precio_total"] ? $this->carrito["precio_total"] : 0;
    }
 	
	public function total_dscto()
    {
	  //si no está definido el elemento total_dscto o no existe el carrito
        //el precio total será 0
        if(!isset($this->carrito["total_dscto"]) || $this->carrito === null)
        {
            return 0;
        }
        //si no es númerico lanzamos una excepción porque no es correcto
        if(!is_numeric($this->carrito["total_dscto"]))
        {
            throw new Exception("El descuento total del carrito debe ser un número", 1);    
        }
        //en otro caso devolvemos el descuento total del carrito
        return $this->carrito["total_dscto"] ? $this->carrito["total_dscto"] : 0;
	}
	
	public function valor_dscto()
    {
	  //si no está definido el elemento valordscto o no existe el carrito
        //el precio total será 0
        if(!isset($this->carrito["valordscto"]) || $this->carrito === null)
        {
            return 0;
        }
        //si no es númerico lanzamos una excepción porque no es correcto
        if(!is_numeric($this->carrito["valordscto"]))
        {
            throw new Exception("El valor del descuento del carrito debe ser un número", 1);    
        }
        //en otro caso devolvemos el descuento total del carrito
        return $this->carrito["valordscto"] ? $this->carrito["valordscto"] : 0;
	}
	
	//total de igv
	public function valor_igv()
    {
	  //si no está definido el elemento valordscto o no existe el carrito
        //el precio total será 0
        if(!isset($this->carrito["igv"]) || $this->carrito === null)
        {
            return 0;
        }
        //si no es númerico lanzamos una excepción porque no es correcto
        if(!is_numeric($this->carrito["igv"]))
        {
            throw new Exception("El valor del igv del carrito debe ser un número", 1);    
        }
        //en otro caso devolvemos el descuento total del carrito
        return $this->carrito["igv"] ? $this->carrito["igv"] : 0;
	}
	
    //método que retorna el número de artículos del carrito
    public function articulos_total()
    {
        //si no está definido el elemento articulos_total o no existe el carrito
        //el número de artículos será de 0
        if(!isset($this->carrito["articulos_total"]) || $this->carrito === null)
        {
            return 0;
        }
        //si no es númerico lanzamos una excepción porque no es correcto
        if(!is_numeric($this->carrito["articulos_total"]))
        {
            throw new Exception("El número de artículos del carrito debe ser un número", 1);    
        }
        //en otro caso devolvemos el número de artículos del carrito
        return $this->carrito["articulos_total"] ? $this->carrito["articulos_total"] : 0;
    }
 
    //este método retorna el contenido del carrito
    public function get_content()
    {
        //asignamos el carrito a una variable
        $carrito = $this->carrito;
        //debemos eliminar del carrito el número de artículos
        //y el precio total para poder mostrar bien los artículos
        //ya que estos datos los devuelven los métodos 
        //articulos_total y precio_total
        unset($carrito["articulos_total"]);
        unset($carrito["precio_total"]);
		unset($carrito["total_dscto"]);
		unset($carrito["valordscto"]);
		unset($carrito["igv"]);
		
        return $carrito == null ? null : $carrito;
    }
	
	public function modificar_cantidad($id,$cant){
		
		if(!empty($this->carrito))
        {
		    $prec = 0;
			$desc = '';
			$idprodusu = '';
			$idunid = 0;
			$descunid = '';
			$dscto = 0;
			$cantconv = 0;
			$cantunidconvt = 0;
			$igv = 0;
			$precio = 0;
			$precioIGV = 'N';
			$paramIGV = 0;
			$subt = 0;
			$PreciosinIGV = 0;
			$idprod = 0;
			$precioUnit = 0;
			$piezas = 0; //10/02/2015
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"]== $id)
                {
					$idprod = $row["id"];
					$prec = $row['precio'];
					$desc = $row['nombre'];
					$idprodusu = $row['idproducto'];
					$idunid = $row['idunidad'];
					$descunid = $row['descunidad'];
					$dscto = $row['descuento'];
					$cantconv = $row['cantconvert'];
					$cantunidconvt = $row['cantunidconv'];
					$paramIGV = $row['ParamIGV'];
					$precioIGV = $row['precioIGV'];
					$PreciosinIGV = $row['PreciosinIGV'];
					$precioUnit = $row['PrecioUnit'];
					
					//10/02/2015 inicio
					$piezas = $row['piezas'];
					//10/02/2015 fin
					//$subt = $subt['subtotal'];
					
					
					//$prec = number_format($prec, 2, '.', ''); 
									//$igv  = $prec * ($paramIGV / 100);
					/*$igv = number_format($igv, 2, '.', ''); 
					
					$igv = $igv * $cant;
					$igv = number_format($igv, 2, '.', ''); 
					*/
					if($cant >=1){
					$subt = ($prec * $cant);
					$subt = number_format($subt, 2, '.', ''); 
					
					$igv = $subt / (($paramIGV / 100) + 1);
					$igv = $igv * ($paramIGV / 100);
					
					$igv = number_format($igv, 2, '.', ''); 
					}
					else{
						$subt = $row['subtotal'];
						$igv = $row['Montoigv']; 
						$subt = number_format($subt, 2, '.', ''); 
						$igv = number_format($igv, 2, '.', ''); 
					}
					
					$cantconv = $cantunidconvt * $cant;
					
					if($cantconv>=1){
						$precioUnit = $subt / $cantconv;
						$precioUnit = number_format($precioUnit, 2, '.', ''); 
					}
					else{
						//$precioUnit = $subt;
						$precioUnit = $subt / $cantconv;
						$precioUnit = number_format($precioUnit, 2, '.', ''); 
					}

					$articulo = array(
        				"id"            =>      $idprod,
				        "cantidad"      =>      $cant,
				        "precio"        =>      $prec,
				        "nombre"        =>      $desc,
						"idproducto"    =>      $idprodusu,
						"idunidad"      =>      $idunid,
						"descunidad"    =>      $descunid,
						"descuento"     =>      $dscto,
						"cantconvert"   =>		$cantconv,
						"cantunidconv"   =>		$cantunidconvt,
						"precioIGV"   =>		$precioIGV,
						"Montoigv"   =>		$igv,
						"ParamIGV"   =>		$paramIGV,
						"subtotal" => $subt,
						"PreciosinIGV" => $PreciosinIGV,
						"PrecioUnit" => $precioUnit
						,"piezas" => $piezas //10/02/2015
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
	} 

public function modificar_precio($id,$prec){
		
		if(!empty($this->carrito))
        {
		    $cant = 0;
			$desc = '';
			$idprodusu = '';
			$idunid = 0;
			$descunid = '';
			$dscto = 0;
			$cantconv = 0;
			$cantunidconvt = 0;
			$igv = 0;
			$precioIGV = 'N';
			$paramIGV = $row['ParamIGV'];
			$subt = 0;
			
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"] == $id)
                {
					$cant = $row['cantidad'];
					$desc = $row['nombre'];
					$idprodusu = $row['idproducto'];
					$idunid = $row['idunidad'];
					$descunid = $row['descunidad'];
					$dscto = $row['descuento'];
					$cantconv = $row['cantconvert'];
					$cantunidconvt = $row['cantunidconv'];
					$igv = $row['Montoigv'];
					$precioIGV = $row['precioIGV'];
					$paramIGV = $row['ParamIGV'];
					
					$igv  = $prec * ($paramIGV / 100);
					$igv = number_format($igv, 2, '.', ''); 
					
					$igv = $igv * $cant;
					$igv = number_format($igv, 2, '.', ''); 
					
					$subt = ($prec * $cant) + $igv;
					$subt = number_format($subt, 2, '.', ''); 
					
					$articulo = array(
        				"id"            =>      $id,
				        "cantidad"      =>      $cant,
				        "precio"        =>      $prec,
				        "nombre"        =>      $desc,
						"idproducto"    =>      $idprodusu,
						"idunidad"      =>      $idunid,
						"descunidad"    =>      $descunid,
						"descuento"     =>      $dscto,
						"cantconvert"   =>		$cantconv,
						"cantunidconv"   =>		$cantunidconvt,
						"precioIGV"   =>		$precioIGV,
						"Montoigv"   =>		$igv,
						"ParamIGV"   =>		$paramIGV,
						"subtotal" => $subt
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
	} 

public function modificar_dscto($id,$dscto){
		
		if(!empty($this->carrito))
        {
		    $cant = 0;
			$desc = '';
			$idprodusu = '';
			$idunid = 0;
			$descunid = '';
			$prec = 0;
			$cantconv = 0;
			$cantunidconvt = 0;
			$igv = 0;
			$precioIGV = 'N';
			$paramIGV = 0;
			$total = 0;
			$subt = 0;
			$prec1 = 0;
			$prec = 0;
			$PreciosinIGV = 0;
			$idprod = 0;
			
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"] == $id)
                {
					$idprod = $row["id"];
					$cant = $row['cantidad'];
					$desc = $row['nombre'];
					$idprodusu = $row['idproducto'];
					$idunid = $row['idunidad'];
					$descunid = $row['descunidad'];
					$prec = $row['precio'];
					$cantconv = $row['cantconvert'];
					$cantunidconvt = $row['cantunidconv'];
					$igv = $row['Montoigv'];
					$precioIGV = $row['precioIGV'];
					$paramIGV = $row['ParamIGV'];
					$precioIGV = 'MODIF';
					$total = $row["total"];
					$PreciosinIGV = $row['PreciosinIGV'];
					
					if($dscto <= 0){ $total = $PreciosinIGV * $cant;
						$total = $total + ($total * ($paramIGV /100));
						$total = number_format($total, 2, '.', '') ; 
					 }
					
					$total = $total - $dscto;
					$prec1 = $total / $cant;
					$prec1 = $prec1 / (($paramIGV /100) + 1);
					
					$prec1 = number_format($prec1, 2, '.', ''); 
					
					$igv = $prec1 * ($paramIGV /100);
					$igv = number_format($igv, 2, '.', ''); 
					
					$igv = $igv * $cant;
					$igv = number_format($igv, 2, '.', ''); 

					$subt = ($prec1 * $cant) + $igv;
					$subt = number_format($subt, 2, '.', ''); 
					
					$articulo = array(
        				"id"            =>      $idprod,
				        "cantidad"      =>      $cant,
				        "precio"        =>      $prec,
				        "nombre"        =>      $desc,
						"idproducto"    =>      $idprodusu,
						"idunidad"      =>      $idunid,
						"descunidad"    =>      $descunid,
						"descuento"     =>      $dscto,
						"cantconvert"   =>		$cantconv,
						"cantunidconv"   =>		$cantunidconvt,
						"precioIGV"   =>		$precioIGV,
						"Montoigv"   =>		$igv,
						"ParamIGV"   =>		$paramIGV,
						"subtotal" => $subt,
						"PreciosinIGV" => $PreciosinIGV
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
	} 


public function modificar_tarifa($id,$tarifa){
		
		if(!empty($this->carrito))
        {
		    $idtipo = 0;
			$cantidad = 0;
			$precio = 0;
			$nombre = '';
			$subtotalhab = 0;
			$mediatarifa = 0;
			$mtarifa = '';
			$cortesia = '';
			$noches = 0;
			$nuevo = '';
			$iddetalle = 0;
			
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"] == $id)
                {
					$idtipo = $row["id"];
					$cantidad = $row['cantidad'];
					$nombre = $row['nombre'];
					$mtarifa = $row['mtarifa'];
					$cortesia = $row['cortesia'];
					$noches = $row['noches'];
					$nuevo =  $row['nuevo'];
					$iddetalle =  $row['iddetalle'];

					if($mtarifa=='SI'){
						$mediatarifa = $tarifa / 2;
					}

					if($cortesia=='SI'){
						$mediatarifa = 0.00;
						$tarifa = 0.00;
						$mtarifa = 'NO';
					}

					$subtotalhab = ($tarifa * $cantidad) * $noches;
	
					$subtotalhab = number_format($subtotalhab, 2, '.', ''); 
					$mediatarifa = number_format($mediatarifa, 2, '.', ''); 
					
					$articulo = array(
        				"id"            =>      $idtipo,
        				"cantidad"      =>      $cantidad,
        				"precio"        =>      $tarifa,
        				"nombre"        =>      $nombre,
						"subtotalhab"    =>     $subtotalhab,
						"mediatarifa"    =>     $mediatarifa,
						"mtarifa"    =>     $mtarifa,
						"cortesia"    =>     $cortesia,
						"noches"    =>     $noches,
						"nuevo"    =>     $nuevo,
						"iddetalle"    =>     $iddetalle
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
	} 

public function modificar_mtarifa($id,$mediatarifa){
		
		if(!empty($this->carrito))
        {
		    $idtipo = 0;
			$cantidad = 0;
			$precio = 0;
			$nombre = '';
			$subtotalhab = 0;
			$mtarifa = '';
			$cortesia = '';
			$noches = 0;
			$tarifa = 0;
			$nuevo = '';
			$iddetalle = 0;
			
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"] == $id)
                {
					$idtipo = $row["id"];
					$cantidad = $row['cantidad'];
					$precio = $row['precio'];
					$nombre = $row['nombre'];
					$mtarifa = $row['mtarifa'];
					$cortesia = $row['cortesia'];
					$noches = $row['noches'];
					$nuevo =  $row['nuevo'];
					$iddetalle =  $row['iddetalle'];

					$mtarifa = 'NO';

					if($mediatarifa> 0){
						$mtarifa = 'SI';
					}


					if($cortesia=='SI'){
						$mediatarifa = 0.00;
						$precio = 0.00;
						$mtarifa = 'NO';
					}

					$subtotalhab = ($precio * $cantidad) * $noches;
	
					$subtotalhab = number_format($subtotalhab, 2, '.', ''); 
					$mediatarifa = number_format($mediatarifa, 2, '.', ''); 
					
					$articulo = array(
        				"id"            =>      $idtipo,
        				"cantidad"      =>      $cantidad,
        				"precio"        =>      $precio,
        				"nombre"        =>      $nombre,
						"subtotalhab"    =>     $subtotalhab,
						"mediatarifa"    =>     $mediatarifa,
						"mtarifa"    =>     $mtarifa,
						"cortesia"    =>     $cortesia,
						"noches"    =>     $noches,
						"nuevo"    =>     $nuevo,
						"iddetalle"    =>     $iddetalle
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
} 

public function modificar_Total($id,$total){
		
		if(!empty($this->carrito))
        {
		    $cant = 0;
			$desc = '';
			$idprodusu = '';
			$idunid = 0;
			$descunid = '';
			$prec = 0;
			$dscto = 0;
			$cantunidconvt = 0;
			$igv = 0;
			$precioIGV = 'N';
			$paramIGV = 0;
			$subt = 0;
			$PreciosinIGV = 0;
			$cantconv = 0;
			$idprod = 0;
			$precioUnit = 0;
			$piezas = 0; //10/02/2015
            foreach ($this->carrito as $row) 
            {
                if($row["unique_id"] == $id)
                {
					$idprod = $row["id"];
					$cant = $row['cantidad'];
					$desc = $row['nombre'];
					$idprodusu = $row['idproducto'];
					$idunid = $row['idunidad'];
					$descunid = $row['descunidad'];
					$prec = $row['precio'];
					$cantconv = $row['cantconvert'];
					$cantunidconvt = $row['cantunidconv'];
					$igv = $row['Montoigv'];
					$precioIGV = $row['precioIGV'];
					$paramIGV = $row['ParamIGV'];
					$subt= $row['subtotal'];
					$PreciosinIGV = $row['PreciosinIGV'];
					//10/02/2015 inicio
					$piezas = $row['piezas'];
					//10/02/2015 fin
					
					if($total<=0){
					 $total = $subt;
					}
					else{
						//////
					if($cant>=1){
						
					$PreciosinIGV = $total / $cant;
					$PreciosinIGV = $PreciosinIGV / (($paramIGV /100) + 1);
					
					$PreciosinIGV = number_format($PreciosinIGV, 2, '.', ''); 
					
					$igv = $PreciosinIGV * ($paramIGV /100);
					
					$igv = number_format($igv, 2, '.', ''); 
					
					$prec = $PreciosinIGV + $igv;
					$igv = $igv * $cant;
					
					$igv = number_format($igv, 2, '.', ''); 
					
					$subt = $total;
					//$subt = ($PreciosinIGV * $cant) + $igv;
					$subt = number_format($subt, 2, '.', ''); 
					$prec = number_format($prec, 2, '.', ''); 	
					
					if($cantconv>0){
					 $precioUnit = $total / $cantconv;
					}
					else{
					 //$precioUnit = $total;
					 $precioUnit = $total / $cantconv;
					}
					
					$precioUnit = number_format($precioUnit, 2, '.', '');				
					
					}
					else{
						//$precioUnit	= $total;
						$precioUnit = $total / $cantconv;
						$precioUnit = number_format($precioUnit, 2, '.', '');	
						$subt = $total; 
						$subt = number_format($subt, 2, '.', ''); 		
					}
						//////
						
					}
					
					$articulo = array(
        				"id"            =>      $idprod,
				        "cantidad"      =>      $cant,
				        "precio"        =>      $prec,
				        "nombre"        =>      $desc,
						"idproducto"    =>      $idprodusu,
						"idunidad"      =>      $idunid,
						"descunidad"    =>      $descunid,
						"descuento"     =>      $dscto,
						"cantconvert"   =>		$cantconv,
						"cantunidconv"  =>		$cantunidconvt,
						"precioIGV"   =>		$precioIGV,
						"Montoigv"   =>		$igv,
						"ParamIGV"   =>		$paramIGV,
						"subtotal" => $subt,
						"PreciosinIGV" => $PreciosinIGV,
						"PrecioUnit" => $precioUnit
						,"piezas" => $piezas //10/02/2015
    				);
					$this->remove_producto($id);
					$this->add($articulo);
					///
                }
            }
        }
	} 
	
    //método que llamamos al insertar un nuevo producto al 
    //carrito para eliminarlo si existia, así podemos insertarlo
    //de nuevo pero actualizado
    private function unset_producto($unique_id)
    {
        unset($_SESSION["carrito"][$unique_id]);
    }
 
    //para eliminar un producto debemos pasar la clave única
    //que contiene cada uno de ellos
    public function remove_producto($unique_id)
    {
        //si no existe el carrito
        if($this->carrito === null)
        {
            throw new Exception("El carrito no existe!", 1);
        }
 
        //si no existe la id única del producto en el carrito
        if(!isset($this->carrito[$unique_id]))
        {
            throw new Exception("La unique_id $unique_id no existe!", 1);
        }
 
        //en otro caso, eliminamos el producto, actualizamos el carrito y 
        //el precio y cantidad totales del carrito
        unset($_SESSION["carrito"][$unique_id]);
        $this->update_carrito();
        $this->update_precio_cantidad();
        return true;
    }
 
    //eliminamos el contenido del carrito por completo
    public function destroy()
    {
        unset($_SESSION["carrito"]);
        $this->carrito = null;
        return true;
    }
 
    //actualizamos el contenido del carrito
    public function update_carrito()
    {
        self::__construct();
    }
 
}