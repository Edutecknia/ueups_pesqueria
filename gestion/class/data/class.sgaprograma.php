<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2015 © 
* @package       - - -
* @name          class.sgaprograma.php
* [Descripcion] :: Clase imagen
 * */
class Data_sgaprograma{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

//Encontrar imagen	
public function fu_listar($conexion,$idSociedad,$nomPrograma,$nomCiudad,$idProgramaTipo,$indActivo,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($nomPrograma)==""){$nomPrograma='NULL';}else{ $nomPrograma= "'" . trim($nomPrograma) . "'"  ;}
			if(trim($nomCiudad)==""){$nomCiudad='NULL';}else{ $nomCiudad= "'" . trim($nomCiudad) . "'"  ;}
			if(trim($idProgramaTipo)==""){$tipoImagenSitio='NULL';}else{ $idProgramaTipo= "'" . trim($idProgramaTipo) . "'"  ;}
			
						
			$sql 	= "CALL USP_PROGRAMA_LISTA_GESTION($nomPrograma, $nomCiudad,$idSociedad,$idProgramaTipo,$indActivo,$paginado,$pagina)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion,$idSociedad,$nomPrograma,$nomCiudad,$idProgramaTipo,$indActivo) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($nomPrograma)==""){$nomPrograma='NULL';}else{ $nomPrograma= "'" . trim($nomPrograma) . "'"  ;}
			if(trim($nomCiudad)==""){$nomCiudad='NULL';}else{ $nomCiudad= "'" . trim($nomCiudad) . "'"  ;}
			if(trim($idProgramaTipo)==""){$tipoImagenSitio='NULL';}else{ $idProgramaTipo= "'" . trim($idProgramaTipo) . "'"  ;}
			
						
			$sql 	= "CALL USP_PROGRAMA_LISTA($nomPrograma, $nomCiudad,$idSociedad,$idProgramaTipo,$indActivo)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_registrar($conexion,$idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $idUsuario, $lugar, $horario, $duracion) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($ciudad)==""){$ciudad='NULL';}else{ $ciudad= "'" . trim($ciudad) . "'"  ;}
			if(trim($fecha)==""){$fecha='NULL';}else{ $fecha= "'" . trim($fecha) . "'"  ;}
			if(trim($desc)==""){$desc='NULL';}else{ $desc= "'" . trim($desc) . "'"  ;}

			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}

			if(trim($lugar)==""){$lugar='NULL';}else{ $lugar= "'" . trim($lugar) . "'"  ;}
			if(trim($horario)==""){$horario='NULL';}else{ $horario= "'" . trim($horario) . "'"  ;}
			if(trim($duracion)==""){$duracion='NULL';}else{ $duracion= "'" . trim($duracion) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_REGISTRAR($idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $idUsuario, $lugar, $horario, $duracion)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editar($conexion,$id, $idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $idUsuario, $lugar, $horario, $duracion) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($ciudad)==""){$ciudad='NULL';}else{ $ciudad= "'" . trim($ciudad) . "'"  ;}
			if(trim($fecha)==""){$fecha='NULL';}else{ $fecha= "'" . trim($fecha) . "'"  ;}
			if(trim($desc)==""){$desc='NULL';}else{ $desc= "'" . trim($desc) . "'"  ;}

			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}

			if(trim($lugar)==""){$lugar='NULL';}else{ $lugar= "'" . trim($lugar) . "'"  ;}
			if(trim($horario)==""){$horario='NULL';}else{ $horario= "'" . trim($horario) . "'"  ;}
			if(trim($duracion)==""){$duracion='NULL';}else{ $duracion= "'" . trim($duracion) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_EDITAR($id, $idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $idUsuario, $lugar, $horario, $duracion)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_registrarDetalle($conexion,$idPrograma,$contenido, $idUsuario, $metodologia, $expositor, $inversion, $informacion) {
		try {

			$contenido 		= str_replace("'",'',$contenido);
			$metodologia 	= str_replace("'",'',$metodologia);
			$expositor 		= str_replace("'",'',$expositor);
			$inversion 		= str_replace("'",'',$inversion);
			$informacion 	= str_replace("'",'',$informacion);

			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}
			if(trim($contenido)==""){$contenido='NULL';}else{ $contenido= "'" . trim($contenido) . "'"  ;}
			if(trim($metodologia)==""){$metodologia='NULL';}else{ $metodologia= "'" . trim($metodologia) . "'"  ;}
			if(trim($expositor)==""){$expositor='NULL';}else{ $expositor= "'" . trim($expositor) . "'"  ;}
			if(trim($inversion)==""){$inversion='NULL';}else{ $inversion= "'" . trim($inversion) . "'"  ;}
			if(trim($informacion)==""){$informacion='NULL';}else{ $informacion= "'" . trim($informacion) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_REGISTRAR_DETALLE($idPrograma, $contenido, $idUsuario, $metodologia, $expositor, $inversion, $informacion)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_registrarOnline($conexion,$idSociedad, $nombre, $orden, $slider, $idUsuario) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_ONLINE_REGISTRAR($idSociedad, $nombre, $orden, $slider, $idUsuario)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarOnline($conexion,$id, $idSociedad, $nombre, $orden, $slider, $idUsuario) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_ONLINE_EDITAR($id, $idSociedad, $nombre, $orden, $slider, $idUsuario)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_inactivar($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_INACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_activar($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_ACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_detallePrograma($conexion,$idPrograma) {
		try {
			
			//if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}

			$sql       	= "SELECT DES_PROGRAMA_DETALLE, DES_PROGRAMA_DETALLE_METODOLOGIA, DES_PROGRAMA_DETALLE_EXPOSITOR, DES_PROGRAMA_DETALLE_INVERSION, DES_PROGRAMA_DETALLE_INFORMACION FROM PROGRAMA_DETALLE WHERE ID_PROGRAMA =  $idPrograma ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_ListarCurso($conexion,$idSociedad, $filtro,$indActivo,$pagina, $idPrograma) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}

			$sql 	= "CALL USP_PROGRAMA_CURSO_LISTA($idSociedad,$filtro,$indActivo,$paginado,$pagina,$idPrograma)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_registrarCurso($conexion,$idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor) {
		try {
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($descripcion)==""){$descripcion='NULL';}else{ $descripcion= "'" . trim($descripcion) . "'"  ;}
			if(trim($contenido)==""){$contenido='NULL';}else{ $contenido= "'" . trim($contenido) . "'"  ;}
			if(trim($finalizar)==""){$finalizar='NULL';}else{ $finalizar= "'" . trim($finalizar) . "'"  ;}
			if(trim($precio)==""){$precio='NULL';}else{ $precio= "'" . trim($precio) . "'"  ;}
			if(trim($duracion)==""){$duracion='NULL';}else{ $duracion= "'" . trim($duracion) . "'"  ;}
			if(trim($modalidad)==""){$modalidad='NULL';}else{ $modalidad= "'" . trim($modalidad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($nomExpositor)==""){$nomExpositor='NULL';}else{ $nomExpositor= "'" . trim($nomExpositor) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO($idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarCurso($conexion,$idCurso, $idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor) {
		try {
			
			if(trim($idCurso)==""){$idCurso='NULL';}else{ $idCurso= "'" . trim($idCurso) . "'"  ;}
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($descripcion)==""){$descripcion='NULL';}else{ $descripcion= "'" . trim($descripcion) . "'"  ;}
			if(trim($contenido)==""){$contenido='NULL';}else{ $contenido= "'" . trim($contenido) . "'"  ;}
			if(trim($finalizar)==""){$finalizar='NULL';}else{ $finalizar= "'" . trim($finalizar) . "'"  ;}
			if(trim($precio)==""){$precio='NULL';}else{ $precio= "'" . trim($precio) . "'"  ;}
			if(trim($duracion)==""){$duracion='NULL';}else{ $duracion= "'" . trim($duracion) . "'"  ;}
			if(trim($modalidad)==""){$modalidad='NULL';}else{ $modalidad= "'" . trim($modalidad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($nomExpositor)==""){$nomExpositor='NULL';}else{ $nomExpositor= "'" . trim($nomExpositor) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO_EDITAR($idCurso, $idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_Encontrar($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_CIUDAD,DESCRIPCION,FECHA,ORDEN_PROGRAMA,SLIDER,NOM_IMAGEN,LUGAR,HORARIO,DURACION FROM PROGRAMA WHERE ID_PROGRAMA =  ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_PROGRAMA"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_PROGRAMA", $row["ID_PROGRAMA"]);
					$obj_sgaprograma->__set("_NOM_PROGRAMA", $row["NOM_PROGRAMA"]);
					$obj_sgaprograma->__set("_NOM_CIUDAD", $row["NOM_CIUDAD"]);
					$obj_sgaprograma->__set("_DESCRIPCION", $row["DESCRIPCION"]);
					$obj_sgaprograma->__set("_FECHA", $row["FECHA"]);
					$obj_sgaprograma->__set("_ORDEN_PROGRAMA", $row["ORDEN_PROGRAMA"]);
					$obj_sgaprograma->__set("_SLIDER", $row["SLIDER"]);
					$obj_sgaprograma->__set("_NOM_IMAGEN", $row["NOM_IMAGEN"]);
					$obj_sgaprograma->__set("_LUGAR", $row["LUGAR"]);
					$obj_sgaprograma->__set("_HORARIO", $row["HORARIO"]);
					$obj_sgaprograma->__set("_DURACION", $row["DURACION"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarCurso($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA_CURSO, ID_PROGRAMA, NOM_CURSO, DES_CURSO, CONTENIDO_CURSO, FINALIZAR_CURSO, PRECIO_CURSO, DURACION_CURSO, MODALIDAD, ORDEN_CURSO,NOM_EXPOSITOR,  ".
					" NOM_IMAGEN, NOM_IMAGEN_SLIDER, NOM_IMAGEN_EXPOSITOR ".
					" FROM PROGRAMA_CURSO WHERE ID_PROGRAMA_CURSO = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_PROGRAMA_CURSO"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_PROGRAMA_CURSO", $row["ID_PROGRAMA_CURSO"]);
					$obj_sgaprograma->__set("_ID_PROGRAMA", $row["ID_PROGRAMA"]);
					$obj_sgaprograma->__set("_NOM_CURSO", $row["NOM_CURSO"]);
					$obj_sgaprograma->__set("_DES_CURSO", $row["DES_CURSO"]);
					$obj_sgaprograma->__set("_CONTENIDO_CURSO", $row["CONTENIDO_CURSO"]);
					$obj_sgaprograma->__set("_FINALIZAR_CURSO", $row["FINALIZAR_CURSO"]);
					$obj_sgaprograma->__set("_PRECIO_CURSO", $row["PRECIO_CURSO"]);
					$obj_sgaprograma->__set("_DURACION_CURSO", $row["DURACION_CURSO"]);
					$obj_sgaprograma->__set("_MODALIDAD", $row["MODALIDAD"]);
					$obj_sgaprograma->__set("_ORDEN_CURSO", $row["ORDEN_CURSO"]);
					$obj_sgaprograma->__set("_NOM_EXPOSITOR", $row["NOM_EXPOSITOR"]);
					$obj_sgaprograma->__set("_NOM_IMAGEN", $row["NOM_IMAGEN"]);
					$obj_sgaprograma->__set("_NOM_IMAGEN_SLIDER", $row["NOM_IMAGEN_SLIDER"]);
					$obj_sgaprograma->__set("_NOM_IMAGEN_EXPOSITOR", $row["NOM_IMAGEN_EXPOSITOR"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_inactivarCurso($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO_INACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_activarCurso($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO_ACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_guardarPdfCurso($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO_PDF($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_ListarRedes($conexion) {
		try {
			
			$sql 	= "SELECT ID_RED_SOCIAL,NOM_RED_SOCIAL,URL_RED_SOCIAL FROM RED_SOCIAL ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_ListarCorreos($conexion) {
		try {
			
			$sql 	= "SELECT ID_CORREO_DESTINO,NOM_CORREO_DESTINO,DIR_CORREO_DESTINO FROM CORREO_DESTINO ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarRedSocial($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_RED_SOCIAL,NOM_RED_SOCIAL,URL_RED_SOCIAL ".
					" FROM RED_SOCIAL WHERE ID_RED_SOCIAL = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_RED_SOCIAL"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_RED_SOCIAL", $row["ID_RED_SOCIAL"]);
					$obj_sgaprograma->__set("_NOM_RED_SOCIAL", $row["NOM_RED_SOCIAL"]);
					$obj_sgaprograma->__set("_URL_RED_SOCIAL", $row["URL_RED_SOCIAL"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarCorreo($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_CORREO_DESTINO,NOM_CORREO_DESTINO,DIR_CORREO_DESTINO  ".
					" FROM CORREO_DESTINO WHERE ID_CORREO_DESTINO = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_CORREO_DESTINO"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_CORREO_DESTINO", $row["ID_CORREO_DESTINO"]);
					$obj_sgaprograma->__set("_NOM_CORREO_DESTINO", $row["NOM_CORREO_DESTINO"]);
					$obj_sgaprograma->__set("_DIR_CORREO_DESTINO", $row["DIR_CORREO_DESTINO"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_guardarUrl($conexion,$id,$url, $idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($url)==""){$url='NULL';}else{ $url= "'" . trim($url) . "'"  ;}

			$sql = "CALL USP_RED_SOCIAL_REGISTRAR($id,$url, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_guardarCorreo($conexion,$id,$correo, $idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($correo)==""){$correo='NULL';}else{ $correo= "'" . trim($correo) . "'"  ;}

			$sql = "CALL USP_CORREO_DESTINO_REGISTRAR($id,$correo, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_mostrarMensaje($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_MENSAJE,USUARIO_ENVIA,DES_MENSAJE,DES_ESTADO,FEC_REGISTRO  ".
					" FROM MENSAJES WHERE USUARIO_RECIBE IS NULL AND DES_ESTADO = "."'".'PENDIENTE'."'".
					" GROUP BY ID_MENSAJE";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_mostrarMensajexId($conexion,$idMensaje,$idUsuario) {
		try {
			$array = array();
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_MOSTRAR_MENSAJE_ID($idMensaje,$idUsuario)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function actualizarMensajes($conexion,$idMensaje) {
		try {
			$array = array();
			
			$sql = "SELECT ID_MENSAJE,USUARIO_ENVIA,DES_MENSAJE,DES_ESTADO,FEC_REGISTRO,TIPO FROM MENSAJES WHERE ID_MENSAJE = ".$idMensaje.' ORDER BY FEC_REGISTRO';
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}		

public function fu_responderMensaje($conexion,$idMensaje,$mensaje, $idUsuario) {
		try {
			if(trim($idMensaje)==""){$idMensaje='NULL';}else{ $idMensaje= "'" . trim($idMensaje) . "'"  ;}
			if(trim($mensaje)==""){$mensaje='NULL';}else{ $mensaje= "'" . trim($mensaje) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_RESPONDER_MENSAJE_CHAT($idMensaje,$mensaje, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_registrarCentro($conexion,$idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $contenido, $idUsuario) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($ciudad)==""){$ciudad='NULL';}else{ $ciudad= "'" . trim($ciudad) . "'"  ;}
			if(trim($fecha)==""){$fecha='NULL';}else{ $fecha= "'" . trim($fecha) . "'"  ;}
			if(trim($desc)==""){$desc='NULL';}else{ $desc= "'" . trim($desc) . "'"  ;}

			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($contenido)==""){$contenido='NULL';}else{ $contenido= "'" . trim($contenido) . "'"  ;}

			if(trim($lugar)==""){$lugar='NULL';}else{ $lugar= "'" . trim($lugar) . "'"  ;}
			if(trim($horario)==""){$horario='NULL';}else{ $horario= "'" . trim($horario) . "'"  ;}
			if(trim($duracion)==""){$duracion='NULL';}else{ $duracion= "'" . trim($duracion) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_REGISTRAR_CENTRO($idSociedad, $nombre, $ciudad, $desc, $fecha, $idProgramaTipo, $orden, $idUsuario, $lugar, $horario, $duracion, $contenido)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarCentro($conexion, $idPrograma, $nomPrograma, $contenido, $ciudad, $idUsuario) {
		try {
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}
			if(trim($ciudad)==""){$ciudad='NULL';}else{ $ciudad= "'" . trim($ciudad) . "'"  ;}
			if(trim($nomPrograma)==""){$nomPrograma='NULL';}else{ $nomPrograma= "'" . trim($nomPrograma) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($contenido)==""){$contenido='NULL';}else{ $contenido= "'" . trim($contenido) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_EDITAR_CENTRO($idPrograma, $nomPrograma, $contenido, $ciudad, $idUsuario)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_eliminar($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_ELIMINAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_eliminarCurso($conexion,$id,$idUsuario) {
		try {
			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_PROGRAMA_CURSO_ELIMINAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarPrograma($conexion,$limite) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 1 AS TIPO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 1 AND IND_ACTIVO = 1 ".
					" UNION ".
					" SELECT ID_PROGRAMA_CURSO, NOM_CURSO, DES_CURSO, CONTENIDO_CURSO, FEC_REGISTRO, NOM_IMAGEN, 2 AS TIPO ".
					" FROM PROGRAMA_CURSO ".
					" WHERE  IND_ACTIVO = 1 ".
					" LIMIT ".$limite;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_totalPrograma($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 1 AS TIPO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 1 AND IND_ACTIVO = 1 ".
					" UNION ".
					" SELECT ID_PROGRAMA_CURSO, NOM_CURSO, DES_CURSO, CONTENIDO_CURSO, FEC_REGISTRO, NOM_IMAGEN, 2 AS TIPO ".
					" FROM PROGRAMA_CURSO ".
					" WHERE  IND_ACTIVO = 1 ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_encontrarPrograma($conexion,$idPrograma) {
		try {
			
			$sql 	= "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_IMAGEN, NOM_CIUDAD,LUGAR,HORARIO,DURACION FROM PROGRAMA WHERE ID_PROGRAMA = ".$idPrograma." AND IND_ACTIVO = 1 ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarGaleria($conexion,$nomGaleria,$indActivo,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			if(trim($nomGaleria)==""){$nomGaleria='NULL';}else{ $nomGaleria= "'" . trim($nomGaleria) . "'"  ;}
						
			$sql 	= "CALL USP_GALERIA_LISTAR($nomGaleria,$indActivo,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}


public function fu_listarGaleriaTodo($conexion) {
		try {
						
			$sql 	= "SELECT 	ID_GALERIA,	NOM_GALERIA,	FEC_REGISTRO, IND_ACTIVO FROM GALERIA WHERE IND_ACTIVO = 1 ORDER BY 1 DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarGaleriaImg($conexion) {
		try {
						
			$sql 	= "SELECT 	ID_GALERIA,	NOM_IMG FROM GALERIA_IMAGEN WHERE IND_ACTIVO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}		

public function fu_registrarGaleria($conexion,$nombre, $idUsuario) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_GALERIA_REGISTRAR($nombre, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_inactivarGaleria($conexion,$id,$idUsuario) {
		try {
			$sql = "UPDATE GALERIA SET IND_ACTIVO = 0  WHERE ID_GALERIA = ".$id;
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}


public function fu_listarComunidad($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 3 AS TIPO, FEC_REGISTRO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 3 AND IND_ACTIVO = 1 ORDER BY ID_PROGRAMA DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_offsetGaleria($conexion,$id) {
		try {
			$sql = "SELECT ID_GALERIA, NOM_GALERIA FROM GALERIA WHERE ID_GALERIA = ".$id;
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}		

public function fu_imagenGaleria($conexion,$id) {
		try {
			$sql = "SELECT ID_GALERIA, NOM_IMG FROM GALERIA_IMAGEN WHERE ID_GALERIA = ".$id." AND IND_ACTIVO = 1";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_eliminarGaleria($conexion,$id) {
		try {
			$sql = "UPDATE GALERIA_IMAGEN SET IND_ACTIVO = 0  WHERE NOM_IMG = "."'".$id."'";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarGaleria($conexion, $id, $nombre, $idUsuario) {
		try {
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}

			$sql = "CALL USP_GALERIA_EDITAR($id, $nombre, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_EncontrarCurso2($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA_CURSO, ID_PROGRAMA, NOM_CURSO, DES_CURSO, CONTENIDO_CURSO, FINALIZAR_CURSO, PRECIO_CURSO, DURACION_CURSO, MODALIDAD, ORDEN_CURSO,NOM_EXPOSITOR,  ".
					" NOM_IMAGEN, NOM_IMAGEN_SLIDER, NOM_IMAGEN_EXPOSITOR ".
					" FROM PROGRAMA_CURSO WHERE ID_PROGRAMA_CURSO = ".$id;

			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarAlianza($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 5 AS TIPO, FEC_REGISTRO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 5 AND IND_ACTIVO = 1 ORDER BY ID_PROGRAMA DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_listarCharla($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 4 AS TIPO, FEC_REGISTRO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 4 AND IND_ACTIVO = 1 ORDER BY ID_PROGRAMA DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}
	
}
?>