<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2015 © 
* @package       - - -
* @name          class.sgaprograma.php
* [Descripcion] :: Clase imagen
 * */
class Data_sgaprograma{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_listar($conexion,$idSociedad,$nomPrograma,$nomCiudad,$idProgramaTipo,$indActivo) {
		try {
			
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($nomPrograma)==""){$nomPrograma='NULL';}else{ $nomPrograma= "'" . trim($nomPrograma) . "'"  ;}
			if(trim($nomCiudad)==""){$nomCiudad='NULL';}else{ $nomCiudad= "'" . trim($nomCiudad) . "'"  ;}
			if(trim($idProgramaTipo)==""){$tipoImagenSitio='NULL';}else{ $idProgramaTipo= "'" . trim($idProgramaTipo) . "'"  ;}
			
						
			$sql 	= "CALL USP_PROGRAMA_LISTA($nomPrograma, $nomCiudad,$idSociedad,$idProgramaTipo,$indActivo)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_listarOnlineSuperior($conexion,$filtro) {
		try {
			
			if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}

			$cadena 	= "AND	(UPPER(NOM_PROGRAMA)	LIKE CONCAT("."'".'%'."'".",IFNULL(UPPER($filtro),UPPER(NOM_PROGRAMA)),"."'".'%'."'"."))";

			$sql       	= "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_IMAGEN FROM PROGRAMA WHERE ID_PROGRAMA_TIPO = 2 AND SLIDER = 1 AND IND_ACTIVO = 1 ".$cadena." ORDER BY ORDEN_PROGRAMA";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarOnlineInferior($conexion,$filtro) {
		try {
			
			if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}

			$cadena 	= "AND	(UPPER(NOM_PROGRAMA)	LIKE CONCAT("."'".'%'."'".",IFNULL(UPPER($filtro),UPPER(NOM_PROGRAMA)),"."'".'%'."'"."))";

			$sql       	= "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_IMAGEN FROM PROGRAMA WHERE ID_PROGRAMA_TIPO = 2 AND SLIDER = 2 AND IND_ACTIVO = 1 ".$cadena." ORDER BY ORDEN_PROGRAMA";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_detallePrograma($conexion,$idPrograma) {
		try {
			
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}

			$sql       	= "SELECT DES_PROGRAMA_DETALLE, DES_PROGRAMA_DETALLE_METODOLOGIA, DES_PROGRAMA_DETALLE_EXPOSITOR, DES_PROGRAMA_DETALLE_INVERSION, DES_PROGRAMA_DETALLE_INFORMACION FROM PROGRAMA_DETALLE WHERE ID_PROGRAMA =  $idPrograma ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Cursos($conexion,$idPrograma) {
		try {
			
			$sql 	= "SELECT ID_PROGRAMA_CURSO,NOM_CURSO,NOM_IMAGEN FROM PROGRAMA_CURSO WHERE ID_PROGRAMA = ".$idPrograma." AND IND_ACTIVO = 1 ORDER BY ORDEN_CURSO";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_nomPrograma($conexion,$idPrograma) {
		try {
			
			if(trim($idPrograma)==""){$idPrograma='NULL';}else{ $idPrograma= "'" . trim($idPrograma) . "'"  ;}

			$sql       	= "SELECT NOM_PROGRAMA FROM PROGRAMA WHERE ID_PROGRAMA =  $idPrograma ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_cursoDetalle($conexion,$idProgramaCurso) {
		try {
			
			if(trim($idProgramaCurso)==""){$idProgramaCurso='NULL';}else{ $idProgramaCurso= "'" . trim($idProgramaCurso) . "'"  ;}

			$sql       	= " SELECT DES_CURSO,CONTENIDO_CURSO,FINALIZAR_CURSO,PRECIO_CURSO,DURACION_CURSO,MODALIDAD,NOM_CURSO,NOM_PDF,NOM_EXPOSITOR,NOM_IMAGEN_SLIDER,NOM_IMAGEN_EXPOSITOR ".
						  " FROM PROGRAMA_CURSO WHERE ID_PROGRAMA_CURSO = ".$idProgramaCurso;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_calcularIdCompra($conexion) {
		try {

			$sql = "CALL USP_CALCULAR_ID_COMPRA()";

			foreach ($conexion->query($sql) as $row):
					$var = $row["CORRELATIVO"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarRedSocial($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_RED_SOCIAL,NOM_RED_SOCIAL,URL_RED_SOCIAL ".
					" FROM RED_SOCIAL WHERE ID_RED_SOCIAL = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_RED_SOCIAL"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_RED_SOCIAL", $row["ID_RED_SOCIAL"]);
					$obj_sgaprograma->__set("_NOM_RED_SOCIAL", $row["NOM_RED_SOCIAL"]);
					$obj_sgaprograma->__set("_URL_RED_SOCIAL", $row["URL_RED_SOCIAL"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarCorreo($conexion,$id) {
		try {
			$array = array();
			
			$sql = "SELECT ID_CORREO_DESTINO,NOM_CORREO_DESTINO,DIR_CORREO_DESTINO  ".
					" FROM CORREO_DESTINO WHERE ID_CORREO_DESTINO = ".$id;

			foreach ($conexion->query($sql) as $row):
				if ($row["ID_CORREO_DESTINO"] != ""):
					$obj_sgaprograma = new Data_sgaprograma();
					$obj_sgaprograma->__set("_ID_CORREO_DESTINO", $row["ID_CORREO_DESTINO"]);
					$obj_sgaprograma->__set("_NOM_CORREO_DESTINO", $row["NOM_CORREO_DESTINO"]);
					$obj_sgaprograma->__set("_DIR_CORREO_DESTINO", $row["DIR_CORREO_DESTINO"]);
				endif;
		
			endforeach;
		return $obj_sgaprograma;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_enviarMensajeChat($conexion,$usuario,$mensaje) {
		try {
			if(trim($usuario)==""){$usuario='NULL';}else{ $usuario= "'" . trim($usuario) . "'"  ;}
			if(trim($mensaje)==""){$mensaje='NULL';}else{ $mensaje= "'" . trim($mensaje) . "'"  ;}

			$sql = "CALL USP_ENVIAR_MENSAJE_CHAT($usuario, $mensaje)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function actualizarMensajes($conexion,$usuario) {
		try {
			$array = array();
			
			$sql = "SELECT ID_MENSAJE,USUARIO_ENVIA,DES_MENSAJE,DES_ESTADO,FEC_REGISTRO,TIPO FROM MENSAJES WHERE USUARIO_ENVIA = "."'".$usuario."'"." OR USUARIO_RECIBE = "."'".$usuario."'"." ORDER BY FEC_REGISTRO ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_listarOnline2($conexion,$suma) {
		try {
			
			//if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}
			//$cadena 	= "AND	(UPPER(NOM_PROGRAMA)	LIKE CONCAT("."'".'%'."'".",IFNULL(UPPER($filtro),UPPER(NOM_PROGRAMA)),"."'".'%'."'"."))";

			$sql       	= "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_IMAGEN FROM PROGRAMA WHERE ID_PROGRAMA_TIPO = 2 AND IND_ACTIVO = 1  ORDER BY ORDEN_PROGRAMA LIMIT ".$suma;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarRedes($conexion) {
		try {
			
			//if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}
			//$cadena 	= "AND	(UPPER(NOM_PROGRAMA)	LIKE CONCAT("."'".'%'."'".",IFNULL(UPPER($filtro),UPPER(NOM_PROGRAMA)),"."'".'%'."'"."))";

			$sql       	= "SELECT ID_RED_SOCIAL,NOM_RED_SOCIAL,URL_RED_SOCIAL FROM RED_SOCIAL";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarOnline_1($conexion) {
		try {
			
		
			$sql        = "SELECT P.ID_PROGRAMA,P.NOM_PROGRAMA,P.SLIDER,P.NOM_IMAGEN ".
            				" FROM PROGRAMA P ".
            				" WHERE P.ID_PROGRAMA_TIPO = 2 AND P.IND_ACTIVO = 1 AND P.SLIDER = 1 ".
            				" ORDER BY P.NOM_PROGRAMA";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarOnline_2($conexion) {
		try {
			
		
			$sql        = "SELECT P.ID_PROGRAMA,P.NOM_PROGRAMA,P.SLIDER,P.NOM_IMAGEN ".
                            " FROM PROGRAMA P ".
                            " WHERE P.ID_PROGRAMA_TIPO = 2 AND P.IND_ACTIVO = 1 AND P.SLIDER = 2 ".
                            " ORDER BY P.NOM_PROGRAMA";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarImagenSitio($conexion,$idSociedad,$tipoImagenSitio,$tipo) {
		try {
			
			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
            if(trim($tipoImagenSitio)==""){$tipoImagenSitio='NULL';}else{ $tipoImagenSitio= "'" . trim($tipoImagenSitio) . "'"  ;}
		
			$sql        = "CALL USP_IMAGEN_SITIO_LISTA($idSociedad, $tipoImagenSitio, $tipo) ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_encontrarPrograma($conexion,$idPrograma) {
		try {
			
			$sql 	= "SELECT ID_PROGRAMA,NOM_PROGRAMA,NOM_IMAGEN, NOM_CIUDAD,LUGAR,HORARIO,DURACION FROM PROGRAMA WHERE ID_PROGRAMA = ".$idPrograma." AND IND_ACTIVO = 1 ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_listarGaleria($conexion) {
		try {
						
			$sql 	= "SELECT 	ID_GALERIA,	NOM_GALERIA,	FEC_REGISTRO, IND_ACTIVO FROM GALERIA WHERE IND_ACTIVO = 1 ORDER BY 1 DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarGaleriaImg($conexion) {
		try {
						
			$sql 	= "SELECT 	ID_GALERIA,	NOM_IMG FROM GALERIA_IMAGEN WHERE IND_ACTIVO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarCharla($conexion) {
		try {
			$array = array();
			
			$sql = "SELECT ID_PROGRAMA, NOM_PROGRAMA, NOM_CIUDAD, DESCRIPCION, FECHA, NOM_IMAGEN, 4 AS TIPO, FEC_REGISTRO ".
					" FROM PROGRAMA ".
					" WHERE ID_PROGRAMA_TIPO = 4 AND IND_ACTIVO = 1 ORDER BY ID_PROGRAMA DESC";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}
	
}
?>