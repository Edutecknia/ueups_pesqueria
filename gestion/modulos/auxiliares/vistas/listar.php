<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
  
<table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th class="center">#</th>
                        <th>Opción</th>
                        <th>Descripción</th>
                        <th class="center">Abrir</th>
                      </tr>
                    </thead>
                    <tbody>

<tr> 
<td align="center">1</td>
<td align="left">Tipos de habitación</td>
<td align="left">Registro de tipos de habitación y tarifas disponibles en el hotel</td>
<td align="center">
<a href="javascript:loadMenu('tipo_habitacion');rutaMenu('Tipos de Habitación','Habitaciones','Tipos Habitación');" class="cursor-point" data-toggle="tooltip" data-placement="top" title="Abrir" ><i class="fa fa-sign-in"></i></a></td>
</tr>

<tr> 
<td align="center">2</td>
<td align="left">Habitaciones</td>
<td align="left">Registro de los números de habitación disponibles en el hotel</td>
<td align="center">
<a href="javascript:loadMenu('habitacion');rutaMenu('Habitaciones','Habitaciones','Habitaciones');" class="cursor-point" data-toggle="tooltip" data-placement="top" title="Abrir" ><i class="fa fa-sign-in"></i></a></td>
</tr> 
                
                    </tbody>
                    
                  </table>
               
</div>                  