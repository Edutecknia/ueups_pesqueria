// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('galeria');rutaMenu('Galeria de Imagenes','Front Desk','Galeria de Imagenes');
}

function listado(){
	var orden = $("#orden").attr("value");
    var direccion=$("#direccion").attr("value");
    var valor = $("#txtBuscar").val();
    var page = $("#pag_actual").attr("value");
	var campo = '';
	var estado= $("#cboEstado").val();
	INI_LOAD();
    $.post("modulos/galeria/galeria.php?cmd=listar", {
        orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		campo: campo
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd 		= "loadCreacion";
    var titulo 		= "";
    var programa 	= '';

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar nueva galeria";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar galeria";
    }
    if (vista == 'curso') {
    	programa 	= $("#"+id).val();
        cmd 		= "loadCreacionCurso";
        titulo 		= "Registrar nuevo curso para " + programa;
		
    }
	if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Programa online";
    }  
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/galeria/galeria.php?cmd=" + cmd, {id: id},
        function(data) {
			//$("#Contenedorform").html(data);
			$("#childModal1").html(data);
			//$("#titulo").html(titulo);
        });

        $("#childModal1").modal({
        });
    });
}

function registrarGaleria(){
	var nombreGaleria 		= $("#txtNombreGal").val();

		INI_LOAD();
    $.post("modulos/galeria/galeria.php?cmd=registrar", {
        nombreGaleria:nombreGaleria
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			listado();			
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
			   QuitarModal();
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}


function editarPrograma(){
	var id 		= $("#txtid").val();
	var nombre = $("#nombrePrograma").val();
	var orden   = $("#txtorden").val();
	var slider   = $("#cboMostrarSlider").val();

		INI_LOAD();
    $.post("modulos/galeria/galeria.php?cmd=editar", {
        id:id,nombre:nombre, slider:slider, orden:orden
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al editar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/galeria/galeria.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/galeria/galeria.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function InactivarImagen(objeto, id){
	
	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){

		$(objeto).parent('td').parent('tr').remove();

        $.post("modulos/galeria/galeria.php?cmd=eliminar", {
        id:id
	    }, function(data) {
    	});	 
			   			
		}
	});
}

function editarGaleria(){
	var nombreGaleria 		= $("#txtNombreGal").val();
	var id 					= $("#txtid").val();

		INI_LOAD();
    $.post("modulos/galeria/galeria.php?cmd=editar", {
        nombreGaleria:nombreGaleria, id:id
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			listado();			
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
			   QuitarModal();
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

z
