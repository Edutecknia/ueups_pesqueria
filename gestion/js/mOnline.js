// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('online');rutaMenu('Programa Online','Front Desk','Programas Online');
}

function listado(){
	var orden = $("#orden").attr("value");
    var direccion=$("#direccion").attr("value");
    var valor = $("#txtBuscar").val();
    var page = $("#pag_actual").attr("value");
	var campo = '';
	var estado= $("#cboEstado").val();
	INI_LOAD();
    $.post("modulos/online/online.php?cmd=listar", {
        orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		campo: campo
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd 		= "loadCreacion";
    var titulo 		= "";
    var programa 	= '';

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar nuevo programa online";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar programa online";
    }
    if (vista == 'curso') {
    	programa 	= $("#"+id).val();
        cmd 		= "loadCreacionCurso";
        titulo 		= "Registrar nuevo curso para " + programa;
		
    }
	if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Programa online";
    }  
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/online/online.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarPrograma(){
	var nombre = $("#nombrePrograma").val();
	var orden   = $("#txtorden").val();
	var slider   = $("#cboMostrarSlider").val();

		INI_LOAD();
    $.post("modulos/online/online.php?cmd=registrar", {
        nombre:nombre, slider:slider, orden:orden
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function editarPrograma(){
	var id 		= $("#txtid").val();
	var nombre = $("#nombrePrograma").val();
	var orden   = $("#txtorden").val();
	var slider   = $("#cboMostrarSlider").val();

		INI_LOAD();
    $.post("modulos/online/online.php?cmd=editar", {
        id:id,nombre:nombre, slider:slider, orden:orden
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al editar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/online/online.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/online/online.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function eliminar(id){
	
	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/online/online.php?cmd=eliminar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro eliminado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al eliminar, revise que el programa no posea cursos.");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}
