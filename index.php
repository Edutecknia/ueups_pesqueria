<?php 
require_once "gestion/config/web.config.php";
include_once APP_DIR . 'autoload.php';

global $conexion;

$modelData              = new Data_sgaprograma();
$arrayProgramma         = $modelData->fu_listarCharla($conexion);

?>

<!doctype html>
 <html class="no-js " lang="es">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Unidad de Extensión y Proyeccion Social</title>
    <!-- Site Icons -->
    <link rel="shortcut icon" href="../web_pesqueria/img/escudo2.png" type="image/x-icon" />
	<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet"> 
	<link href="http://allfont.es/allfont.css?fonts=franklin-gothic-heavy" rel="stylesheet" type="text/css" />
    <!-- Custom & Default Styles -->
	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="style.css">

	<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
	<![endif]-->

</head>
<body>  

    <!-- LOADER 
    <div id="preloader">
        <img class="preloader" src="images/loader.gif" alt="">
    </div>end loader -->
    <!-- END LOADER -->

    <div id="wrapper">
        <!-- BEGIN # MODAL LOGIN -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Begin # DIV Form -->
                    <div id="div-forms">
                        <form id="login-form">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="flaticon-add" aria-hidden="true"></span>
                            </button>
                            <div class="modal-body">
                                <input class="form-control" type="text" placeholder="What you are looking for?" required>
                            </div>
                        </form><!-- End # Login Form -->
                    </div><!-- End # DIV Form -->
                </div>
            </div>
        </div>
        <!-- END # MODAL LOGIN -->

        <header class="header">
           
        <?php include('nav.php') ?>
        </header>

        <section id="home" class="video-section js-height-full">
            <div class="overlay"></div>
            <div class="home-text-wrapper relative container">
                <div class="home-message">
                   <br>
                    <p>Unidad de Extensión Universitaria y Proyección Social</p>
                   <div class="btn-wrapper">
                       <br>
                       <br>
                        <div class="text-center">
                    <a href="#nosotros" class="btn btn-primary wow slideInLeft">Conócenos</a> &nbsp;&nbsp;&nbsp;
                        </div>
                    </div> 
                </div>
            </div>
            <div class="slider-bottom">
                <span>Bienvenido<i class="fa fa-angle-down"></i></span>
            </div>
        </section>

        <section class="section" id="nosotros">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 hidden-sm hidden-xs">
                        <div class="custom-module">
                            <img src="images/referencia_ueups.jpg" alt="" class="img-responsive wow slideInLeft">
                        </div><!-- end module -->
                    </div><!-- end col -->
                    <div class="col-md-8 wow slideInLeft">
                       <br>
                        <div class="custom-module p40l">
                            <h2 style="color:#155c8f;">SOBRE LA UNIDAD:</h2>
                            <p style="text-align:justify;color:#155c8f">
                             La Unidad de Extensión Universitaria y Proyección Social de la Facultad de Pesquería (UEUPS-FAPE) es parte del Sistema de Extensión Universitaria y de Proyección social de la UNALM y está encargada de brindar apoyo, seguimiento y evaluación de las prácticas pre-profesionales para la obtención del crédito de prácticas de los alumnos de la facultad, coordina y hace seguimiento a las capacitaciones brindadas por los docentes de la facultad, realiza seguimiento de los viajes de intercambio de los alumnos y docentes y promueve actividades de proyección social para alumnos y público en general.   
                            </p>
                         
                        </div><!-- end module -->
                       
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <section class="section gb" id="extension">
            <div class="container">
                <div class="section-title text-center">
                    <h3 style="color:#155c8f">EXTENSIÓN UNIVERSITARIA</h3>
             <p>Aquí Encontrarás todos nuestros cursos presenciales y futuras charlas</p>
                </div><!-- end title -->
                
                <div class="row blog-grid shop-grid">
                <div id="ContenedorListado"></div>      
                </div><!-- end row -->
                 <div class="section-button text-center">
                    <a href="javascript:cargarProgramas();" class="btn btn-primary">Ver Más Cursos</a>
                    </div>
            </div><!-- end container -->
        </section>
        
	 <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3 style="color:#155c8f;">CHARLAS</h3>
                </div><!-- end title -->

                 <?php if(count($arrayProgramma) > 0){ ?>

          <div id="owl-01" class="owl-carousel owl-theme owl-theme-01">

          <?php foreach ($arrayProgramma as $obj): ?>

                <div class="caro-item">
                        <div class="course-box">
                            <div class="image-wrap entry">
                                <img src="img/actualizacion_presencial/<?php echo $obj['NOM_IMAGEN']; ?>" alt="" class="img-responsive">
                                <div class="magnifier">
                                     <a href="detalle_charla.php?cmd=<?php echo $obj['ID_PROGRAMA']; ?>" title=""><i class="fa fa-eye"></i></a>
                                </div>
                            </div><!-- end image-wrap -->
                            <div class="course-details">
                                <h4>
                                    <a href="detalle_charla.php?cmd=<?php echo $obj['ID_PROGRAMA']; ?>" title=""><?php echo $obj['NOM_PROGRAMA']; ?></a>
                                </h4>
                            </div><!-- end details -->
                           <div class="course-footer clearfix">
                                <div class="pull-left">
                                    <ul class="list-inline">
                                        <li><a><i class="fa fa-calendar"></i> <?php echo $obj['NOM_CIUDAD']; ?></a></li>
                                    </ul>
                                </div><!-- end left -->
                            </div><!-- end footer -->
                        </div><!-- end box -->
                    </div><!-- end col -->

            <?php endforeach; ?>

          </div>
          <?php  }  ?>

                
                
            </div><!-- end container -->
        </section>        
        
        
        <section class="section gb" id="proy_social">
          <div class="section-title text-center">
            <h3 style="color:#155c8f">PROYECCIÓN SOCIAL</h3>
             <p>Aquí Encontrarás nuestras listas de alianzas, voluntariados y visita a comunidades</p>
           </div><!-- end title -->  
           <div class="container">
               <div class="row">
                   <div class="col-md-4">
                   <div class="course-box">
                    <center>
                        <div>
                        <a href="alianza.php">
                          <span>
                          <b>
                 <i class="fa fa-handshake-o" aria-hidden="true" style="color:#155c8f;font-size:150px"></i></b>
                  <h3 style="color:#155c8f">Alianzas</h3>         
                          </span>  
                        </a>
                        
                        </div>
                    </center>    
                   </div>
                      
                   </div>
                   <div class="col-md-4">
                   <div class="course-box">
                    <center>
                        <div>
                        <a href="https://www.facebook.com/GAV.UNALM/" target="_blank">
                          <span>
                          <b>
                 <i class="fa fa-user" aria-hidden="true" style="color:#155c8f;font-size:150px"></i></b>
                  <h3 style="color:#155c8f">Voluntariados</h3>         
                          </span>  
                        </a>
                        
                        </div>
                    </center>    
                   </div>
                      
                   </div>
                   <div class="col-md-4">
                   <div class="course-box">
                    <center>
                        <div>
                        <a href="comunidad.php">
                          <span>
                          <b>
                 <i class="fa fa-home" aria-hidden="true" style="color:#155c8f;font-size:150px"></i></b>
                  <h3 style="color:#155c8f">Visita a Comunidades</h3>         
                          </span>  
                        </a>
                        
                        </div>
                    </center>    
                   </div>
                      
                   </div>
               </div>
           </div>
           
        </section>

    <div  style="background:rgba(0,0,0,0.7) !important">
      <footer class="section footer noover">
       <?php include('footer.php') ?>
    </footer>  
    </div>
    
    </div><!-- end wrapper -->
    
  

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/custom.js"></script>
    <!-- VIDEO BG PLUGINS -->
    <script src="js/videobg.js"></script>


    <script type="text/javascript">
    $(document).ready(function() {

    cargarProgramas();

});

    </script>    

</body>
</html>