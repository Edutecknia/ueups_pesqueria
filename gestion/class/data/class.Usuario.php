<?php

/**
 *
 * @author        
 * @copyright     2015 © 
 * @package       class/data/
 * @name          class.Usuario.php
 *
 * [Descripcion] :: Clase Usuario
 * */
class Data_Usuario {

 
    protected $_sess_usu_id = '';
    protected $_sess_usu_name = '';
    protected $_sess_usu_nomc = '';
	protected $_sess_usu_idcli = '';
	
	protected $_sess_cli='';
	protected $_sess_nomsis='';
	protected $_sess_nomemp='';
	protected $_sess_logemp='';
	
    public function __construct() {    
    }

    public function __get($propiedad) {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor) {
        $this->$propiedad = $valor;
    }

 

    public function asignar_datos($user_id,$user,$idsociedad) {
        try {
                    $objUsuario = new Data_Usuario();
                    $objUsuario->__set('_sess_usu_id', $user_id);
                    $objUsuario->__set('_sess_usu_name', $user);
					$objUsuario->__set('_sess_usu_idsociedad', $idsociedad);
                    $flag = $objUsuario;
               
            		return $flag;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

}


?>