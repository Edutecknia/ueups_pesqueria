<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Documento de Identidad</th>
                        <th>E-mail</th>
                        <th>Usuario</th>
                        <th class="center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayUsuario as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <td><?php echo $obj['NOM_USUARIO'].' '.$obj['APE_USUARIO'];?></td>
                    <td><?php echo $obj['ABR_DOCUMENTO_IDENTIDAD'].':'.$obj['NUM_DOCUMENTO_IDENTIDAD'];?></td>
                    <td><?php echo $obj['DES_EMAIL'];?></td>
                    <td><?php echo $obj['NOM_LOGIN'];?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar" 
                        onclick="javascript:OpenForm('edicion','<?php echo $obj['ID_USUARIO'] ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Permisos" 
                        onclick="javascript:OpenFormPermisos('permisos','<?php echo $obj['ID_USUARIO'] ?>','<?php echo $obj['NOM_LOGIN'] ?>');">
                        <i class="fa fa-users"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm('visualizar','<?php echo $obj['ID_USUARIO'] ?>');">
                        <i class="fa fa-eye"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Activo - Click para inactivar" 
                        onclick="javascript:Inactivar('<?php echo $obj['ID_USUARIO'] ?>');">
                        <i class="fa fa-check-circle"></i>
                    </a>
                    <?php
                    }
                    else{
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Inactivo - Click para activar" 
                        onclick="javascript:Activar('<?php echo $obj['ID_USUARIO'] ?>');">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  