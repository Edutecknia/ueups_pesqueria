﻿                     <?php if(count($arrayProgramma) > 0){ ?>

                      <?php
                        $contador = 0;
                
                        foreach ($arrayProgramma as $obj):
                        $contador++;

                        ?>

                            <div class="content blog-list">
                                <div class="blog-wrapper clearfix">
                                    <div class="blog-meta">
                                        <small><a href="#">Comunidades</a></small>
                                        <h3><a href="blog-single.html" title=""><?php echo $obj['NOM_PROGRAMA']; ?></a></h3>
                                        <ul class="list-inline">
                                            <li><?php echo date('d-m-Y H:i', strtotime($obj['FEC_REGISTRO'])) ?></li>
                                            <li><span>redactado por</span> <a href="#">UEUPS</a></li>
                                        </ul>
                                    </div><!-- end blog-meta -->

                                    <div class="blog-media">
                                        <a href="detalle_comunidad.php?cmd=<?php echo $obj['ID_PROGRAMA'] ?>" title=""><img src="img/actualizacion_presencial/<?php echo $obj['NOM_IMAGEN']; ?>" alt="" class="img-responsive img-rounded"></a>
                                    </div><!-- end media -->

                                    <div class="blog-desc-big">
                                        <!--<p class="lead">You can get all the icon versions by checking out our standard license that come with every free icons..</p>
                                        <p>Integer eu urna sit amet dolor fringilla vulputate. Sed diam nunc, pellentesque sed lobortis non, tincidunt et sem. Sed sollicitudin elementum mi eget lobortis. Aliquam molestie rhoncus nisl, vitae molestie leo imperdiet ac. Aliquam diam est, aliquam vitae tristique nec, pretium a libero. Vivamus tempor sed turpis sit amet malesuada.</p>-->
                                        <a href="blog-single.html" class="btn btn-primary">Ver más</a>
                                    </div><!-- end desc -->
                                </div><!-- end blog -->
                            </div><!-- end content -->

                            <?php
                            endforeach;
                            ?>

                    <?php } ?>    