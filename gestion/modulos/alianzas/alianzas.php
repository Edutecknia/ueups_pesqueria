<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = '';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($campo,$orden,$direccion,$pagina,$estado,$filtro){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_Listar($conexion,$idSociedad, $filtro, '', 5, $estado,$pagina);

	foreach ($arrayPrograma as $obj):
        $numTotalElementos = $obj['FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var orden = $("#orden").val();
							var direccion = $("#direccion").val();
							var valor = $("#txtBuscar").val();
							var campo = "";
							var estado=$("#cboEstado").val();
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/alianzas/alianzas.php?cmd=listar", {
						orden:orden, direccion:direccion, valor:valor,pagina:num,estado:estado,campo:campo
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	include $nombreModulo . 'vistas/crear.php';
}

function registrar($nombre, $ciudad, $fecha, $desc, $orden, $contenido){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_registrarCentro($conexion,$idSociedad, $nombre, $ciudad, $desc, $fecha, 5, $orden, $contenido, $idUsuario); 

	if($rpta>='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function loadDetalle($idPrograma){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	$idPrograma	= $idPrograma;
	$modelPrograma 			= new Data_sgaprograma();
	$objDetalle			 	= $modelPrograma->fu_detallePrograma($conexion,$idPrograma);
	include $nombreModulo . 'vistas/detalle.php';
}

function registrardetalle($idPrograma,$contenido){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelPrograma 	= new Data_sgaprograma();
	
	$rpta 			= $modelPrograma->fu_registrarDetalle($conexion,$idPrograma,$contenido, $idUsuario); 

	if($rpta	==	'1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$modelPrograma 			= new Data_sgaprograma();
	$objDetalle			 	= $modelPrograma->fu_detallePrograma($conexion,$id);

	$obj_sgaprograma = new Data_sgaprograma();
	$obj_sgaprograma = $obj_sgaprograma->fu_Encontrar($conexion,$id);

	$id = $id;
	include $nombreModulo . 'vistas/editar.php';

}

function editar($id, $nombre, $ciudad, $fecha, $desc, $orden, $contenido){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_editarCentro($conexion,$id, $nombre, $contenido, $ciudad, $idUsuario); 

	if($rpta>='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function loadVisualizar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$modelPrograma 			= new Data_sgaprograma();
	$objDetalle			 	= $modelPrograma->fu_detallePrograma($conexion,$id);

	$obj_sgaprograma = new Data_sgaprograma();
	$obj_sgaprograma = $obj_sgaprograma->fu_Encontrar($conexion,$id);

	$id = $id;
	$indVisualizar = 1;
	include $nombreModulo . 'vistas/editar.php';

}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_inactivar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_activar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}

function eliminar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_eliminar($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['campo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['estado'],$_POST['valor']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['nombre'],$_POST['ciudad'],$_POST['fecha'],$_POST['desc'],$_POST['orden'],$_POST['contenido']);break;			
	case 'loadDetalle':
		loadDetalle($_POST['id']);break;
	case 'registrardetalle':
		registrardetalle($_POST['idPrograma'],$_POST['contenido']);break;
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;	
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;		
	case 'editar':
		editar($_POST['id'],$_POST['nombre'],$_POST['ciudad'],$_POST['fecha'],$_POST['desc'],$_POST['orden'],$_POST['contenido']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'eliminar':
		eliminar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>