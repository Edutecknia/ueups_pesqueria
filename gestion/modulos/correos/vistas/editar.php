           <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="titulo"></h4>
                    </div>
					
                    <form action="javascript:editarCorreo();" id="frm">
                      <div class="modal-body">
                     <input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>                                    
<div class="form-group">
		<label>Tipo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
<input name="txtdescrip" id="txtdescrip" type="text" class="form-control " placeholder="Ingrese Descripción" readonly="readonly" value="<?php echo $objPrograma->__get('_NOM_CORREO_DESTINO'); ?>">                                  
                                </div>
                      </div>


<div class="form-group">
    <label>Correo destino:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
<input name="txtcorreo" id="txtcorreo" type="text" class="form-control " maxlength="500" placeholder="digite los correos de destino separados por coma (,)" value="<?php echo $objPrograma->__get('_DIR_CORREO_DESTINO'); ?>">                                  
                                </div>
                      </div>
                 
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>
						
                        </div>
                    </form>
               </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->