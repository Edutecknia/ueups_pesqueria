<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = '';
	$direccion = 'ASC';

	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_listarTodo($conexion,$idSociedad, '', '', 2, '1');

    include $nombreModulo . 'vistas/main.php';
}


function listar($campo,$orden,$direccion,$pagina,$estado,$filtro, $idPrograma){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_ListarCurso($conexion,$idSociedad, $filtro, $estado, $pagina, $idPrograma);

	foreach ($arrayPrograma as $obj):
        $numTotalElementos = $obj['FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO);

	include $nombreModulo . 'vistas/listar.php';
	
	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var orden = $("#orden").val();
							var direccion = $("#direccion").val();
							var valor = $("#txtBuscar").val();
							var campo = "tiph_descripcion";
							var estado=$("#cboEstado").val();
							var idPrograma = $("#cboPrograma").val();
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/curso/curso.php?cmd=listar", {
						orden:orden, direccion:direccion, valor:valor,pagina:num,estado:estado,campo:campo,idPrograma:idPrograma
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();
	$arrayPrograma 	= $modelPrograma->fu_listarTodo($conexion,$idSociedad, '', '', 2, '1');

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden,$nomExpositor){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_registrarCurso($conexion,$idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor); 

	if($rpta=='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();	
	$arrayPrograma 	= $modelPrograma->fu_listarTodo($conexion,$idSociedad, '', '', 2, '1');
	$objPrograma 	= $modelPrograma->fu_EncontrarCurso($conexion,$id);

	$id = $id;

	include $nombreModulo . 'vistas/editar.php';

}

function loadVisualizar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['USUARIO'];
	$idSociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$modelPrograma 	= new Data_sgaprograma();	
	$arrayPrograma 	= $modelPrograma->fu_listarTodo($conexion,$idSociedad, '', '', 2, '1');
	$objPrograma 	= $modelPrograma->fu_EncontrarCurso($conexion,$id);

	$id = $id;

	include $nombreModulo . 'vistas/visualizar.php';

}

function editar($idCurso, $idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden,$nomExpositor){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_editarCurso($conexion,$idCurso, $idPrograma, $nombre, $descripcion, $contenido, $finalizar, $precio, $duracion, $modalidad, $orden, $idUsuario,$nomExpositor); 

	if($rpta=='1'){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_inactivarCurso($conexion,$id,$idUsuario); 

	if($rpta>=1){echo 'passed';}
	else{ echo 'failed';}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_activarCurso($conexion,$id,$idUsuario); 

	if($rpta>=1){echo 'passed';}
	else{ echo 'failed';}
}

function loadPDF($idCurso){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelPrograma 	= new Data_sgaprograma();
	$objPrograma 	= $modelPrograma->fu_EncontrarCurso($conexion,$idCurso);
	$idCurso		= $idCurso;

	include $nombreModulo . 'vistas/pdf.php';

}

function subirpdf($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_guardarPdfCurso($conexion,$id,$idUsuario); 

	if($rpta>=1){echo 'passed';}
	else{ echo 'failed';}
}

function eliminar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idSociedad 	= $objUsuario->__get('_sess_usu_idsociedad');

	$modelPrograma 	= new Data_sgaprograma();

	$rpta =$modelPrograma->fu_eliminarCurso($conexion,$id,$idUsuario); 

	if($rpta=='1'){echo 'passed';}
	else{ echo 'failed';}
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['campo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['estado'],$_POST['valor'],$_POST['idPrograma']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['idPrograma'],$_POST['nombre'],$_POST['descripcion'],$_POST['contenido'],$_POST['finalizar'],$_POST['precio'],$_POST['duracion'],$_POST['modalidad'],$_POST['orden'],$_POST['nomExpositor']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['idCurso'],$_POST['idPrograma'],$_POST['nombre'],$_POST['descripcion'],$_POST['contenido'],$_POST['finalizar'],$_POST['precio'],$_POST['duracion'],$_POST['modalidad'],$_POST['orden'],$_POST['nomExpositor']);break;			
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'loadPDF':
		loadPDF($_POST['id']);break;	
	case 'subirpdf':
		subirpdf($_POST['idCurso']);break;	
	case 'eliminar':
		eliminar($_POST['id']);break;	
    default:
        vistaPrincipal();
        break;
}
?>