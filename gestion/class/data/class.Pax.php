<?php
class Data_Pax
{
 
    //aquí guardamos el contenido del carritopax
    private $carritopax = array();
 
    //seteamos el carritopax exista o no exista en el constructor
    public function __construct()
    {
        
        if(!isset($_SESSION["carritopax"]))
        {
            $_SESSION["carritopax"] = null;
        }
        $this->carritopax = $_SESSION['carritopax'];
    }
 
    //añadimos un producto al carritopax
    public function add($articulo = array())
    {
        //primero comprobamos el articulo a añadir, si está vacío o no es un 
        //array lanzamos una excepción y cortamos la ejecución
        if(!is_array($articulo) || empty($articulo))
        {
            throw new Exception("Error, el articulo no es un array!", 1);    
        }
 
        //nuestro carro necesita siempre un id producto, cantidad y precio articulo || !$articulo["precio"]
        if(!$articulo["id"] || !$articulo["cantidad"])
        {
            throw new Exception("Error, el articulo debe tener un id, cantidad y precio!", 1);    
        }
 
        //nuestro carro necesita siempre un id producto, cantidad y precio articulo
        /*if(!is_numeric($articulo["id"]) || !is_numeric($articulo["cantidad"]) || !is_numeric($articulo["precio"]))
        {
            throw new Exception("Error, el id, cantidad y precio deben ser números!", 1);    
        }*/
 
        //debemos crear un identificador único para cada producto
        //$unique_id = $articulo["id"];//md5($articulo["id"]);
 
        //creamos la id única para el producto
        //$articulo["unique_id"] = $unique_id;
        
        //si no está vacío el carritopax lo recorremos 
        if(!empty($this->carritopax))
        {
			$nid = 0;
            foreach ($this->carritopax as $row) 
            {
                //comprobamos si este producto ya estaba en el 
                //carritopax para actualizar el producto o insertar
                //un nuevo producto    
				
				if($row["unique_id"] !=""){
					$nid = $nid += 1;
				}  

                //if($row["unique_id"] === $unique_id)
                //{
					
                    //si ya estaba sumamos la cantidad
                    //$articulo["cantidad"] = $row["cantidad"] + $articulo["cantidad"];
                //}
            }
			 $nid = $nid += 1;
			 //verificar si id no existen en carritopax
			 $flag = 1;
			 while ($flag = 1) {
			 
			 $flag = $this->verificar($nid);
			 if($flag==1){ //si id generado aumentar en 1 y volver a verificar
				$nid = $nid += 1;	
			 }
			 else{ break;}
			 
			 }
			 
			 $unique_id = $nid; 
			 $articulo["unique_id"] = $unique_id;
        }
		else{
			 $unique_id = $nid += 1;
			 $articulo["unique_id"] = $unique_id;
		}
 
        //evitamos que nos pongan números negativos y que sólo sean números para cantidad y precio
        //$articulo["cantidad"] = trim(preg_replace('/([^0-9\.])/i', '', $articulo["cantidad"]));
        //$articulo["precio"] = trim(preg_replace('/([^0-9\.])/i', '', $articulo["precio"]));
 		
        //añadimos un elemento total al array carritopax para 
        //saber el precio total de la suma de este artículo


        //primero debemos eliminar el producto si es que estaba en el carritopax
        //$this->unset_producto($unique_id);
 		
        ///ahora añadimos el producto al carritopax
        $_SESSION["carritopax"][$unique_id] = $articulo;
 
        //actualizamos el carritopax
        $this->update_carritopax();

    }
 
 public function verificar($nid){
	
	$flag = 0;
	foreach ($this->carritopax as $row) 
    {
			 if($row["unique_id"] === $nid)
			 {
			  $flag = 1;
			  break;	   
			 }			  
	}
	return $flag;
	/*while ( $cuenta <= 10) {
		echo "Cuenta vale $cuenta <br>";
		$cuenta++;
	}*/
		
}

    //este método retorna el contenido del carritopax
    public function get_content()
    {
        //asignamos el carritopax a una variable
        $carritopax = $this->carritopax;
        return $carritopax == null ? null : $carritopax;
    }

    //método que llamamos al insertar un nuevo producto al 
    //carritopax para eliminarlo si existia, así podemos insertarlo
    //de nuevo pero actualizado
    private function unset_producto($unique_id)
    {
        unset($_SESSION["carritopax"][$unique_id]);
    }
 
    //para eliminar un producto debemos pasar la clave única
    //que contiene cada uno de ellos
    public function remove_pax($unique_id)
    {
        //si no existe el carritopax
        if($this->carritopax === null)
        {
            throw new Exception("El carritopax no existe!", 1);
        }
 
        //si no existe la id única del producto en el carritopax
        if(!isset($this->carritopax[$unique_id]))
        {
            throw new Exception("La unique_id $unique_id no existe!", 1);
        }
 
        //en otro caso, eliminamos el producto, actualizamos el carritopax y 
        //el precio y cantidad totales del carritopax
        unset($_SESSION["carritopax"][$unique_id]);
        $this->update_carritopax();
        //$this->update_precio_cantidad();
        return true;
    }
 
    //eliminamos el contenido del carritopax por completo
    public function destroy()
    {
        unset($_SESSION["carritopax"]);
        $this->carritopax = null;
        return true;
    }
 
    //actualizamos el contenido del carritopax
    public function update_carritopax()
    {
        self::__construct();
    }
 
}