<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2015 © 
* @package       - - -
 * */
class Data_sgausuario{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_login($conexion,$nomusu,$passwd) {
		try {

			if(trim($nomusu)==""){$nomusu='NULL';}else{ $nomusu= "'" . trim($nomusu) . "'"  ;}
			if(trim($passwd)==""){$passwd='NULL';}else{ $passwd= "'" . trim(md5($passwd)) . "'"  ;}
						
			$sql = "CALL USP_USUARIO_LOGIN($nomusu,$passwd)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["CHECK_CONTRASENA"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_encontrar($conexion,$idUsuario) {
		try {
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
						
			$sql = "CALL USP_USUARIO_ENCONTRAR($idUsuario)";
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			//fetchAll
			/*foreach ($conexion->query($sql) as $row):
					$objsga_usuario = new Data_sgausuario();
					$objsga_usuario->__set("_empresa_id", $row["empresa_id"]);
			endforeach;
		return $objsga_usuario;*/
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_estadoConexion($conexion,$usuario) {
		try {
			
			$sql       	= "SELECT IND_CONEXION FROM USUARIO WHERE NOM_LOGIN = "."'".$usuario."'";
		
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_actualizarConexion($conexion,$nomusu) {
		try {
			if(trim($nomusu)==""){$nomusu='NULL';}else{ $nomusu= "'" . trim($nomusu) . "'"  ;}
						
			$sql = "CALL USP_USUARIO_CONEXION($nomusu)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_listar($conexion,$idSociedad,$nomUsuario,$indActivo,$pagina) {
		try {

			$paginado = TAM_PAG_LISTADO;

			if(trim($idSociedad)==""){$idSociedad='NULL';}else{ $idSociedad= "'" . trim($idSociedad) . "'"  ;}
			if(trim($nomUsuario)==""){$nomUsuario='NULL';}else{ $nomUsuario= "'" . trim($nomUsuario) . "'"  ;}
			
						
			$sql 	= "CALL USP_USUARIO_LISTA_GESTION($nomUsuario,$indActivo,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_listarTipoDocumento($conexion) {
		try {
			
			$sql 	= "SELECT ID_DOCUMENTO_IDENTIDAD,NOM_DOCUMENTO_IDENTIDAD,ABR_DOCUMENTO_IDENTIDAD FROM DOCUMENTO_IDENTIDAD WHERE IND_ACTIVO = 1 ORDER BY 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_usuarioRegistrar($conexion,$nomUsuario, $apeUsuario, $nomLogin, $idDocumentoIdentidad, $numDocumentoIdentidad, $desMail,$idUsuario) {
		try {
			if(trim($nomUsuario)==""){$nomUsuario='NULL';}else{ $nomUsuario= "'" . trim($nomUsuario) . "'"  ;}
			if(trim($apeUsuario)==""){$apeUsuario='NULL';}else{ $apeUsuario= "'" . trim($apeUsuario) . "'"  ;}
			if(trim($nomLogin)==""){$nomLogin='NULL';}else{ $nomLogin= "'" . trim($nomLogin) . "'"  ;}
			if(trim($numDocumentoIdentidad)==""){$numDocumentoIdentidad='NULL';}else{ $numDocumentoIdentidad= "'" . trim($numDocumentoIdentidad) . "'"  ;}
			if(trim($desMail)==""){$desMail='NULL';}else{ $desMail= "'" . trim($desMail) . "'"  ;}
						
			$sql = "CALL USP_USUARIO_REGISTRAR($nomUsuario, $apeUsuario, $nomLogin, $idDocumentoIdentidad, $numDocumentoIdentidad, $desMail,$idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_usuarioEditar($conexion,$id, $nomUsuario, $apeUsuario, $nomLogin, $idDocumentoIdentidad, $numDocumentoIdentidad, $desMail,$idUsuario) {
		try {
			if(trim($nomUsuario)==""){$nomUsuario='NULL';}else{ $nomUsuario= "'" . trim($nomUsuario) . "'"  ;}
			if(trim($apeUsuario)==""){$apeUsuario='NULL';}else{ $apeUsuario= "'" . trim($apeUsuario) . "'"  ;}
			if(trim($nomLogin)==""){$nomLogin='NULL';}else{ $nomLogin= "'" . trim($nomLogin) . "'"  ;}
			if(trim($numDocumentoIdentidad)==""){$numDocumentoIdentidad='NULL';}else{ $numDocumentoIdentidad= "'" . trim($numDocumentoIdentidad) . "'"  ;}
			if(trim($desMail)==""){$desMail='NULL';}else{ $desMail= "'" . trim($desMail) . "'"  ;}
						
			$sql = "CALL USP_USUARIO_EDITAR($id, $nomUsuario, $apeUsuario, $nomLogin, $idDocumentoIdentidad, $numDocumentoIdentidad, $desMail,$idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_usuarioActivar($conexion,$id,$idUsuario) {
		try {
			
			$sql = "CALL USP_USUARIO_ACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_usuarioInactivar($conexion,$id,$idUsuario) {
		try {
			
			$sql = "CALL USP_USUARIO_INACTIVAR($id, $idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoListar($conexion,$id) {
		try {
						
			$sql 	= "select ID_SISTEMA_PRIVILEGIO from USUARIO_SISTEMA_PRIVILEGIO WHERE ID_USUARIO = ".$id;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoEliminar($conexion,$id) {
		try {
			 
       $sql = "CALL USP_USUARIO_PERMISO_ELIMINAR($id)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;

		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoRegistrar($conexion,$id,$idPermiso,$idUsuario) {
		try {
	
		$sql = "CALL USP_USUARIO_PERMISO_REGISTRAR($id,$idPermiso,$idUsuario)";

			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;

		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarClave($conexion,$id,$passwd,$idUsuario) {
		try {
		
		if(trim($passwd)==""){$passwd='NULL';}else{ $passwd= "'" . trim(md5($passwd)) . "'"  ;}

		$sql = "CALL USP_USUARIO_EDITAR_CLAVE($id,$passwd,$idUsuario)";
		
			foreach ($conexion->query($sql) as $row):
					$var = $row["IND_OPERACION"];
			endforeach;

		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}
	
}
?>