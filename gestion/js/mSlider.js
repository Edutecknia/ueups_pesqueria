// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('slider');rutaMenu('Slider Principal','Front Desk','Slider Principal');
}

function listado(){
	var orden = $("#orden").attr("value");
    var direccion=$("#direccion").attr("value");
    var valor = $("#txtBuscar").val();
    var page = $("#pag_actual").attr("value");
	var campo = '';
	var estado= $("#cboEstado").val();
	INI_LOAD();
    $.post("modulos/slider/slider.php?cmd=listar", {
        orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		campo: campo
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar nuevo slider";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar slider";
		
    }
  
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/slider/slider.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarSlider(){
	var orden = $("#txtorden").val();
		INI_LOAD();
    $.post("modulos/slider/slider.php?cmd=registrar", {
        orden:orden
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/slider/slider.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/slider/slider.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Visualizar(id){

	    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/slider/slider.php?cmd=visualizar", {id: id},
        function(data) {
			$("#childModal1").html(data);
        });
        
        $("#childModal1").modal({
        });
    });

}