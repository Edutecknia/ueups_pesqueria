<script>
$(document).ready(function(){
 $('#dtfllegada').datetimepicker(
   {
     format: 'DD/MM/YYYY',
     defaultDate: new Date()
   }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
  });   
  
  //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
  
});
</script>

<link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/custompresencial.js?v=1.0.5"></script>

<form action="javascript:editarPrograma();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

 <input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  

  <div class="form-group col-sm-12">
                <label>Nombre del Programa:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombrePrograma" name="nombrePrograma" 
                                            class="form-control" 
                                            placeholder="Ingrese nombre del programa" 
                                            type="text" 
                                            maxlength="100" 
                                            required="required"
                                            value="<?php echo $obj_sgaprograma->__get('_NOM_PROGRAMA'); ?>">
                                </div>
                        </div>

<div class="form-group col-sm-3">
    <label>Fecha:</label>
  <div class='input-group date' id='dtfllegada'>
<input type='text' class="form-control" id="nomCiudad" value="<?php echo $obj_sgaprograma->__get('_NOM_CIUDAD'); ?>" />
                   <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                      </div>

<!--<div class="form-group col-sm-3">
                <label>Fecha:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomCiudad" name="nomCiudad" 
                                            class="form-control" 
                                            placeholder="Ingrese fecha del programa" 
                                            type="text" 
                                            maxlength="100" 
                                            required="required"
                                            value="<?php //echo $obj_sgaprograma->__get('_NOM_CIUDAD'); ?>">
                                </div>
                        </div>  -->


<div class="form-group col-sm-3">
                <label>Lugar:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtLugar" name="txtLugar" 
                                    class="form-control" 
                                    placeholder="Ingrese lugar del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_sgaprograma->__get('_LUGAR'); ?>">
                                </div>
                        </div> 


<div class="form-group col-sm-3">
                <label>Horario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtHorario" name="txtHorario" 
                                    class="form-control" 
                                    placeholder="Ingrese horario del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_sgaprograma->__get('_HORARIO'); ?>">
                                </div>
                        </div> 

<div class="form-group col-sm-3">
                <label>Horas del Curso:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDuracion" name="txtDuracion" 
                                    class="form-control" 
                                    placeholder="Horas de duración del programa" 
                                    type="number" step="1"
                                    maxlength="5" 
                                    required="required"
                                    value="<?php echo $obj_sgaprograma->__get('_DURACION'); ?>">
                                </div>
                        </div>                         

<div class="form-group  col-sm-3">
                <label>Orden de Ubicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtorden" 
                                          name="txtorden" 
                                          class="form-control" 
                                          placeholder="Ingrese valor" 
                                          type="number" 
                                          step="1" 
                                          required="required"
                                          value="<?php echo $obj_sgaprograma->__get('_ORDEN_PROGRAMA'); ?>">
                                </div>
                        </div>

<!--<div class="form-group col-sm-4">
    <label>Fecha Programa:</label>
          <div class='input-group date' id='dtfllegada'>-->  
                          <input type='hidden' 
                                class="form-control" 
                                id="fechaPrograma" 
                                required="required"
                                value="<?php echo date('d/m/Y', strtotime($obj_sgaprograma->__get('_FECHA'))); ?>"
                                 />
                    <!--<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                      </div>  -->                                           


<!--<div class="form-group col-sm-12">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="desPrograma" name="desPrograma" 
                                    class="form-control" 
                                    placeholder="Descripción del programa" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_sgaprograma->__get('_DESCRIPCION'); ?>">
                                </div>
                        </div> -->


<div class="tab-content col-sm-12" style="background-color:#FFF;">
      <div class="tab-pane fade active in" id="subirfoto">
      

         <div class="form-group col-sm-12">
         <center>
         <input type="hidden" name="txtidcarpeta" id="txtidcarpeta" value="EDIT_<?php echo $id;?>"/>
          <label>Medidas(1920x1080px)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input2" multiple>
                                </div>
                                </center>
                      </div>


            
            <div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid">
            <li class="jFiler-item" data-jfiler-index="0" style="">                        
            <div class="jFiler-item-container">                            
            <div class="jFiler-item-inner">                                
            <div class="jFiler-item-thumb">                                    
            <div class="jFiler-item-status"></div>                                    
            <div class="jFiler-item-info">                                        
           <span class="jFiler-item-title"><b title="<?php echo $obj_sgaprograma->__get('_NOM_IMAGEN'); ?>"><?php echo $obj_sgaprograma->__get('_NOM_IMAGEN'); ?></b></span> 
            <span class="jFiler-item-others">KB</span>                                 
            </div>                                    
            <div class="jFiler-item-thumb-image"><img src="../img/actualizacion_presencial/<?php echo $obj_sgaprograma->__get('_NOM_IMAGEN'); ?>" draggable="false"></div>                                
            </div>                                
            <div class="jFiler-item-assets jFiler-row">                                    
            <ul class="list-inline pull-left">                                        
            <li><div class="jFiler-jProgressBar" style="display: none;"><div class="bar" style="width: 100%;">
            </div></div><div class="jFiler-item-others text-success"><i class="icon-jfi-check-circle"></i> Correcto</div></li>                                    
            </ul>                                    
            <ul class="list-inline pull-right">                                        
            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>                                    
            </ul>                                
            </div>                            
            </div>                        
            </div>                    
            </li></ul></div>

                      
                      
      </div>
      </div>


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<?php if($indVisualizar != 1 ){?>                            
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
<?php } ?>
                        </div>

 </div>
</form>         




                   