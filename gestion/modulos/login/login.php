<?php
require_once "../../config/web.config.php";
set_time_limit(0);
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';
/**
 * Carga la vista principal
 */
function vistaPrincipal($bd) {
   	
   	$_SESSION['pDB']['dbName'] = trim($emp);
	$_SESSION['BDEX']=trim($emp);
	$_SESSION['BDNOM']= trim($emp);
	
	$objConexion = new Data_DB();
	$conexion = $objConexion->conn();

    include $nombreModulo . 'vistas/main.php';
}

function login($usu,$pass){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	$objsga_usuario 	= new Data_sgausuario();
	$objUsuario 		= new Data_Usuario();
	$idUsuario		 	= $objsga_usuario->fu_login($conexion,$usu,$pass);

	if($idUsuario > 0){
	
	$usuario				= $objsga_usuario->fu_encontrar($conexion,$idUsuario);
	$objUsuario 			= $objUsuario->asignar_datos($usuario['ID_USUARIO'], $usu, $usuario['ID_SOCIEDAD']);
	$_SESSION['USUARIO'] 	= $objUsuario;  
	$rpta = 'passed';  
	}
	else{
	$rpta = 'Error al ingresar';
	}
	echo $rpta;
}

function datosInicio(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');
	$idsociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$objsga_Sociedad 	= new Data_sgasociedad();
    $sociedad 			= $objsga_Sociedad->fu_encontrar($conexion, $idsociedad);
	
	$nomcorto 			= $sociedad['NOM_CORTO'];
	
	echo $nomcorto.'|'.$usucre;
}

function validarConexion($timestamp){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');
	$idsociedad		= $objUsuario->__get('_sess_usu_idsociedad');
	
	$conexion_actual = 1;
	$conexion_bd	 = 1;

	while( $conexion_bd == $conexion_actual )
	{	
		$objsga_Usuario 	= new Data_sgausuario();
    	$filas 				= $objsga_Usuario->fu_estadoConexion($conexion, $usucre);

		usleep(100000);//anteriormente 10000
		clearstatcache();
		foreach ($filas as $obj):
    		$conexion_bd  = $obj['IND_CONEXION'];
    	endforeach;	
	}

		$objsga_Usuario 	= new Data_sgausuario();
    	$filas 				= $objsga_Usuario->fu_estadoConexion($conexion, $usucre);

    	foreach ($filas as $obj):
    		$ar["IND_CONEXION"] 	 = $obj['IND_CONEXION'];	
    	endforeach;

		$dato_json   = json_encode($ar);
		echo $dato_json;

}

function actualizarConexion($variable){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');

	$objsga_Usuario 	= new Data_sgausuario();
    $var 				= $objsga_Usuario->fu_actualizarConexion($conexion, $usucre);

    echo $var;
}

function mostrarMensajes(){

	global $conexion, $etiquetaTitulo, $nombreModulo;

		$objsga_Mensajes 	= new Data_sgaprograma();
    	$filas 				= $objsga_Mensajes->fu_mostrarMensaje($conexion);
    	$cadena				= '';
    	$contador			= 0;
    	foreach ($filas as $obj):

    		$contador++;

    		$mensaje 	= $obj['DES_MENSAJE'];
    		$idmensaje 	= $obj['ID_MENSAJE'];
    		$usuario 	= $obj['USUARIO_ENVIA'];

    		if(strlen($mensaje) > 20){
    			$mensaje = substr($mensaje,0,20);
    			$mensaje.= '...';
    		}

    		$cadena.= '<li><a href="javascript:chatWith('.$idmensaje.','.$idmensaje.');"><div class="pull-left"><img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"></div>'.
    				  '<h4>Visitante '.$idmensaje.'</h4><p>'.$mensaje.'</p></a></li>';
    		/*
			<li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
    		*/
    		/*$ar["ID_MENSAJE"] 	 	= $obj['_ID_MENSAJE'];
    		$ar["USUARIO_ENVIA"] 	= $obj['_USUARIO_ENVIA'];
    		$ar["DES_MENSAJE"] 	 	= $obj['_DES_MENSAJE'];	*/
    	endforeach;

    	echo $cadena;

}

function aceptarMensajexId($idMensaje,$usuario){

	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');

	$objsga_Mensajes 	= new Data_sgaprograma();
    	$filas 			= $objsga_Mensajes->fu_mostrarMensajexId($conexion,$idMensaje,$usucre);

    $cadena				= '';

    foreach ($filas as $obj):

    	$mensaje 	= $obj['DES_MENSAJE'];

    	$cadena.= '<div class="chatboxmessage"><span class="chatboxmessagefrom">'.$usuario.':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'.$mensaje.'</span></div>';

    endforeach;
	echo $cadena;

}

function actualizarMensaje($idMensaje){

	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');

	$objsga_Mensajes 	= new Data_sgaprograma();
    	$filas 			= $objsga_Mensajes->actualizarMensajes($conexion,$idMensaje);

    $cadena				= '';

    foreach ($filas as $obj):

    	$mensaje 	= $obj['DES_MENSAJE'];
    	$usuario 	= $obj['USUARIO_ENVIA'];
    	$tipo 		= $obj['TIPO'];

    	if($tipo == 'VISITANTE'){
    		$usuario = 'VISITANTE_'.$idMensaje;
    	}
    	$cadena.= '<div class="chatboxmessage"><span class="chatboxmessagefrom">'.$usuario.':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'.$mensaje.'</span></div>';

    endforeach;
	echo $cadena;

}

function responderMensaje($destino,$mensaje){

	global $conexion, $etiquetaTitulo, $nombreModulo;
	$objUtils = new Logic_Utils();
	
	$objUsuario 	= $_SESSION['USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');

	$arreglo 		= @explode('_', $destino);
	$idMensaje 		= $arreglo[1];

	$mensaje = $objUtils->limpiar_cadena($mensaje);

	$objsga_Mensajes 	= new Data_sgaprograma();
    $rpta	 			= $objsga_Mensajes->fu_responderMensaje($conexion,$idMensaje,$mensaje,$usucre);

}

function cerrarSesion(){
	$objSession = new Data_Session();
	$objSession->validaSession();
	exit(0);
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
	case 'main':
		 vistaPrincipal($_POST['bd']);
		 break;	
	case 'login':
		 login($_POST['usu'],$_POST['pas']);
		 break;		
	case 'datosInicio':
		 datosInicio();
		 break;
	case 'validarConexion':
		 validarConexion($_POST['timestamp']);
		 break;	 
	case 'actualizarConexion':
		 actualizarConexion($_POST['tipo']);
		 break;
	case 'mostrarMensajes':
		 mostrarMensajes();
		 break;	 
	case 'aceptarMensajexId':
		 aceptarMensajexId($_POST['idMensaje'],$_POST['usuario']);
		 break;
	case 'actualizarMensaje':
		 actualizarMensaje($_POST['idMensaje']);
		 break;	
	case 'responderMensaje':
		 responderMensaje($_POST['to'],$_POST['message']);
		 break;
	case 'cerrarSesion':
		 cerrarSesion();
		 break;
    default:
        vistaPrincipal();
        break;
}
?>