  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">-->
  <link rel="stylesheet" href="css/css_editor/froala_editor.css">
  <link rel="stylesheet" href="css/css_editor/froala_style.css">
  <link rel="stylesheet" href="css/css_editor/plugins/code_view.css">
  <link rel="stylesheet" href="css/css_editor/plugins/colors.css">
  <link rel="stylesheet" href="css/css_editor/plugins/emoticons.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image_manager.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image.css">
  <link rel="stylesheet" href="css/css_editor/plugins/line_breaker.css">
  <link rel="stylesheet" href="css/css_editor/plugins/table.css">
  <link rel="stylesheet" href="css/css_editor/plugins/char_counter.css">
  <link rel="stylesheet" href="css/css_editor/plugins/video.css">
  <link rel="stylesheet" href="css/css_editor/plugins/fullscreen.css">
  <link rel="stylesheet" href="css/css_editor/plugins/file.css">
  <link rel="stylesheet" href="css/css_editor/plugins/quick_insert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

  <script type="text/javascript" src="js/js_editor/froala_editor.min.js" ></script>
  <script type="text/javascript" src="js/js_editor/plugins/align.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/colors.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/entities.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/file.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/link.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/lists.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quote.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/table.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/save.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/url.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/video.min.js"></script>
  <script type="text/javascript" src="js/js_editor/languages/es.js"></script>

  <script>
    $(function(){
      $('#edit').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })
    });

    $(function(){
      $('#edit2').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })
    });

    $(function(){
      $('#edit3').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })
    });

    $(function(){
      $('#edit4').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })
    });

    $(function(){
      $('#edit5').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })
    });


    $(document).ready(function(){
        
        $(".fr-hidden").removeClass("fr-hidden")

        var allDivs = $('div');
        var topZindex = 0;
        var topDivId;
          allDivs.each(function(){
            var currentZindex = parseInt($(this).css('z-index'));
          if(currentZindex == 9999) {
              $(this).css('z-index',-9999);
          }
      });
    
    });

  </script>

<form action="javascript:registrarDetallePrograma();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">


<div id="editor">
<label>PRESENTACIÓN:</label>
<div id='edit' style="margin-top: 30px; min-height:200px;">
<?php 
foreach ($objDetalle as $obj):
echo $obj['DES_PROGRAMA_DETALLE'];
endforeach;
?>
</div>
</div>

<br>
<div id="editor2">
<label>METODOLOGÍA:</label>
<div id='edit2' style="margin-top: 30px; min-height:200px;">
<?php 
foreach ($objDetalle as $obj):
echo $obj['DES_PROGRAMA_DETALLE_METODOLOGIA'];
endforeach;
?>
</div>
</div>


<br>
<div id="editor3">
<label>EXPOSITOR:</label>
<div id='edit3' style="margin-top: 30px; min-height:200px;">
<?php 
foreach ($objDetalle as $obj):
echo $obj['DES_PROGRAMA_DETALLE_EXPOSITOR'];
endforeach;
?>
</div>
</div>

<br>
<div id="editor4">
<label>INVERSIÓN:</label>
<div id='edit4' style="margin-top: 30px; min-height:200px;">
<?php 
foreach ($objDetalle as $obj):
echo $obj['DES_PROGRAMA_DETALLE_INVERSION'];
endforeach;
?>
</div>
</div>

<br>
<div id="editor5">
<label>INFORMACIÓN:</label>
<div id='edit5' style="margin-top: 30px; min-height:200px;">
<?php 
foreach ($objDetalle as $obj):
echo $obj['DES_PROGRAMA_DETALLE_INFORMACION'];
endforeach;
?>
</div>
</div>

</div>
<input type="hidden" name="idPrograma" id="idPrograma" value="<?php echo $idPrograma; ?>">


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>




                   