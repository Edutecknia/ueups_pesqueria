<?php

require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';


session_start();
if($_SESSION['USUARIO']==NULL){
session_destroy();
echo "<script>sessionExpire();</script>";   
exit(0);
}

global $conexion;
  
  $objUsuario   = $_SESSION['USUARIO'];
  $idUsuario    = $objUsuario->__get('_sess_usu_id');

    //si es administrador, asignar todos los permisos
  if($idUsuario == 1000){

    $array=array();

    for ($i=0; $i <= 100; $i++) { 
      $array[] = $i;  
    }

  }
  else{

        $modelUsuario   = new Data_sgausuario();
        $arrayPermisos  = $modelUsuario->fu_permisoListar($conexion,$idUsuario); 
 
        $array=array();
        if(count($arrayPermisos)>0){

        foreach ($arrayPermisos as $obj):
    
          $array[] = $obj['ID_SISTEMA_PRIVILEGIO'];
    
        endforeach;
      
        }

  }


?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UNIDAD DE RESPONSABILIDAD SOCIAL UNIVERSITARIA</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   
   <?php
   include 'librerias_css.php';
   ?> 
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini "> <!--sidebar-collapse-->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">URSU</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg" id="nomemp">URSU</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegaci&oacute;n</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu" onclick="javascript:mostrarMensajes();">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success" id="idCantMensajes">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header" id="idCantMensajes2">Tienes 0 mensajes nuevos</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu" id="itemsMensaje">
                      
                      <!-- start message -->
                      <!--<li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>-->
                      <!-- end message -->

                    </ul>
                  </li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
             
              <!-- Tasks: style can be found in dropdown.less -->
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="divusuario1">UserName</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p id="divusuario2">
                      UserName
                      <!--<small>Member since Nov. 2012</small>-->
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--<div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Perfil</a>
                    </div>-->
                    <div class="pull-right">
                      <a href="javascript:cerrarSesion();" class="btn btn-default btn-flat">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p id="divusuario">UserName</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!--<form class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">OPCIONES DE MENU</li>
            <!--active-->
            <?php if(in_array(1,$array) || in_array(2,$array) || in_array(3,$array) || in_array(4,$array) || in_array(5,$array)
            || in_array(6,$array) || in_array(7,$array)){ ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Panel de Opciones</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array(1,$array)){ ?>
                <!--<li><a href="javascript:loadMenu('slider');rutaMenu('Slider Principal','Front Desk','Slider Principal');"><i class="fa fa-circle-o"></i>Slider Principal</a></li>-->
                <?php } ?>

                <?php if(in_array(2,$array)){ ?>
                <li><a href="javascript:loadMenu('presencial');rutaMenu('Cursos Presenciales','Front Desk','Cursos Presenciales');"><i class="fa fa-circle-o"></i>Cursos Presenciales</a></li>
                <?php } ?>
                
                <?php if(in_array(3,$array)){ ?>
                <li><a href="javascript:loadMenu('online');rutaMenu('Categoria Virtuales','Front Desk','Categoria Virtuales');"><i class="fa fa-circle-o"></i>Categoria Virtuales</a></li>
                <?php } ?>

                <?php if(in_array(4,$array)){ ?>
                <li><a href="javascript:loadMenu('curso');rutaMenu('Cursos Virtuales','Front Desk','Cursos Virtuales');"><i class="fa fa-circle-o"></i>Cursos Virtuales</a></li>
                <?php } ?>

                <?php if(in_array(5,$array)){ ?>
                <li><a href="javascript:loadMenu('centro');rutaMenu('Comunidades','Front Desk','Comunidades');"><i class="fa fa-circle-o"></i>Comunidades</a></li>
                <?php } ?>

		<?php if(in_array(2,$array)){ ?>
                <li><a href="javascript:loadMenu('charlas');rutaMenu('Charlas','Front Desk','Charlas');"><i class="fa fa-circle-o"></i>Charlas</a></li>
                <?php } ?>
		
		<?php if(in_array(5,$array)){ ?>
                <li><a href="javascript:loadMenu('alianzas');rutaMenu('Alianzas','Front Desk','Alianzas');"><i class="fa fa-circle-o"></i>Alianzas</a></li>
                <?php } ?>

                <?php /*if(in_array(6,$array)){*/ ?>
               <!-- <li><a href="javascript:loadMenu('red_social');rutaMenu('Redes Sociales','Front Desk','Redes Sociales');"><i class="fa fa-circle-o"></i>Redes Sociales</a></li>-->
                <?php /*}*/ ?>

                <?php if(in_array(7,$array)){ ?>
                <!--<li><a href="javascript:loadMenu('correos');rutaMenu('Correos de destino','Front Desk','Correos');"><i class="fa fa-circle-o"></i>Correos</a></li>-->
                <?php } ?>

                <li><a href="javascript:loadMenu('galeria');rutaMenu('Galeria de Fotos','Front Desk','Galeria de Fotos');"><i class="fa fa-circle-o"></i>Galeria de Fotos</a></li>
              </ul>
            </li>
            <?php } ?>

            <?php if(in_array(8,$array)){ ?>
            <!--<li>
              <a href="javascript:loadMenu('usuario');rutaMenu('Usuarios','Inicio','Usuarios');">
                <i class="fa fa-users"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php } ?>
            <li>
              <a href="javascript:loadMenu('clave');rutaMenu('Cambiar Contraseña','Inicio','Cambiar Contraseña');">
                <i class="fa fa-unlock-alt"></i> <span>Cambiar Contraseña</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 id="namemodulo">
            <!--<small>Control panel</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" id="homeruta"><i class=""></i></a></li>
            <li class="active" id="nameruta"></li>
          </ol>
        </section>

        <!-- CONTENEDOR PRINCIPAL -->
       <section class="content">
       	<div class="row">
         <section class="col-lg-12"> 
         <div id="ContentPrincipal">     
          Bienvenido          
         </div>
         </section><!-- right col -->
         </div>
        </section><!-- /.FIN CONTENEDOR PRINCIPAL -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; <?php echo date('Y').' ' ?> <a href="#" target="_blank"></a>.</strong> Todos los derechos reservados.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

	<?php
    include 'librerias_js.php';
	?>
    
  </body>
  <script>
$(document).ready(function(){
	ObtenerDatosInicio();		
});
</script>
</html>

<div id="main_container">

</div>