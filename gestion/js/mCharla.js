// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('charlas');rutaMenu('Charlas','Front Desk','Charlas');
}

function listado(){
	var orden = $("#orden").attr("value");
    var direccion=$("#direccion").attr("value");
    var valor = $("#txtBuscar").val();
    var page = $("#pag_actual").attr("value");
	var campo = '';
	var estado= $("#cboEstado").val();
	INI_LOAD();
    $.post("modulos/charlas/charlas.php?cmd=listar", {
        orden: orden, direccion: direccion, pagina: page, valor: valor,estado:estado,
		campo: campo
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar nueva charla";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar charla";
    }
    if (vista == 'detalleCurso') {
        cmd = "loadDetalle";
        titulo = "Detalle de la charla";
    }
    if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Programa charla";
    }
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/charlas/charlas.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarPrograma(){
	var nombre = $("#nombrePrograma").val();
	var ciudad = $("#nomCiudad").val();
	var fecha  = $("#fechaPrograma").val();
	var desc   = $("#desPrograma").val();
	var orden   = $("#txtorden").val();

	var lugar    = $("#txtLugar").val();
	var horario  = $("#txtHorario").val();
	var duracion = $("#txtDuracion").val();

		INI_LOAD();
    $.post("modulos/charlas/charlas.php?cmd=registrar", {
        nombre:nombre, ciudad:ciudad, fecha:fecha, desc:desc, orden:orden, lugar:lugar, 
        horario:horario, duracion:duracion
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function registrarDetallePrograma(){
	var idPrograma 	= $("#idPrograma").val();
	//var contenido	= $(".fr-view").html();
	var contenido 	= $("#edit .fr-view").html();
	var metodologia = $("#edit2 .fr-view").html();
	var expositor   = $("#edit3 .fr-view").html();
	var inversion 	= $("#edit4 .fr-view").html();
	var informacion = $("#edit5 .fr-view").html();

		INI_LOAD();
    $.post("modulos/charlas/charlas.php?cmd=registrardetalle", {
        idPrograma:idPrograma, contenido:contenido, metodologia:metodologia, expositor:expositor, inversion:inversion, informacion:informacion 
    }, function(data) {
        if (data=="passed") {
			//$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();

			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
			location.reload();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });

}

function editarPrograma(){
	var id 		= $("#txtid").val();
	var nombre 	= $("#nombrePrograma").val();
	var ciudad 	= $("#nomCiudad").val();
	var fecha  	= $("#fechaPrograma").val();
	var desc   	= $("#desPrograma").val();
	var orden   = $("#txtorden").val();

	var lugar    = $("#txtLugar").val();
	var horario  = $("#txtHorario").val();
	var duracion = $("#txtDuracion").val();

		INI_LOAD();
    $.post("modulos/charlas/charlas.php?cmd=editar", {
        id:id, nombre:nombre, ciudad:ciudad, fecha:fecha, desc:desc, orden:orden, lugar:lugar, 
        horario:horario, duracion:duracion
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al editar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/charlas/charlas.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/charlas/charlas.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function eliminar(id){
	
	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/charlas/charlas.php?cmd=eliminar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro eliminado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al eliminar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}