<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>  
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Tipo</th>
                        <th>Correo Destino</th>
                        <th class="center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($arrayPrograma as $obj):
					?>
                    <tr>
                    <td><?php echo $obj['NOM_CORREO_DESTINO'];?></td>
                    <td><?php echo $obj['DIR_CORREO_DESTINO'];?></td>
                    <td class="center" align="center">
                     <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar correos" 
                        onclick="javascript:OpenForm('<?php echo $obj['ID_CORREO_DESTINO'] ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>              
</div>                  