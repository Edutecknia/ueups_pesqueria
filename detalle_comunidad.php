<?php
require_once "gestion/config/web.config.php";
include_once APP_DIR . 'autoload.php';


  global $conexion;

  $cadena       = $_GET['cmd'];

  $cadena = str_replace(
            array("|",
                 "'",
                 "&"),
            '', $cadena);

 if(!is_numeric($cadena)){
    exit(0);
 }
                     
            $modelData              = new Data_sgaprograma();
            $headerPrograma         = $modelData->fu_encontrarPrograma($conexion,$cadena);
            $DetallePrograma        = $modelData->fu_detallePrograma($conexion,$cadena);

            foreach ($headerPrograma as $obj):

             $nomimg    = $obj['NOM_IMAGEN'];
             $nomcurso  = $obj['NOM_PROGRAMA'];
             $fecha     = $obj['NOM_CIUDAD'];
             $lugar     = $obj['LUGAR'];
             $horario   = $obj['HORARIO'];
             $duracion  = $obj['DURACION'];

             endforeach;

            foreach ($DetallePrograma as $obj):

             $presentacion    = $obj['DES_PROGRAMA_DETALLE'];
             endforeach;

?>

<!doctype html>
 <html class="no-js " lang="es">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Unidad de Extensión Universitaria y Proyección Social</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
     <link rel="shortcut icon" href="../web_pesqueria/img/escudo2.png" type="image/x-icon" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet"> 
    	<link href="http://allfont.es/allfont.css?fonts=franklin-gothic-heavy" rel="stylesheet" type="text/css" />
    <!-- Custom & Default Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="style.css">

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

</head>
<body>  



    <div id="wrapper">

        <!-- END # MODAL LOGIN -->
        
         <header class="header">
           
        <?php include('nav.php') ?>
        
        </header>
        <section class="panel-detalle">
             <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="tagline-message page-title">
                            <h3><?php echo $nomcurso; ?></h3>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        
        <section class="section" style="padding:0px">
        <div class="barra_horizontal">
          <div class="container">
          <center>
          <ul class="nav nav-tabs" style="visibility:hidden">
          <li class="active"><a data-toggle="tab" href="#presentacion">Presentación</a></li>
          <li><a data-toggle="tab" href="#pl_estudios">Plan de Estudios</a></li>
          <li><a data-toggle="tab" href="#docentes">Docentes</a></li>
          <li><a data-toggle="tab" href="#inversion">Inversión</a></li>
          <li><a data-toggle="tab" href="#inscripcion">Proceso de Inscripción</a></li>
          <li><a data-toggle="tab" href="#info">Solicitar Información</a></li>
          </ul>  
          </center>
             
          </div>
           
        </div>
           <section class="" style="padding:2rem 0">
               
           
            <div class="container">
            <div class="row"> 
            <br>  
             
            <div class="col-md-8 col-sm-8">
              <div >
                <div class="tab-content">
              <div id="presentacion" class="tab-pane fade in active">
                <h3>Presentacion</h3>
                <p><?php echo $presentacion; ?></p>
              </div>
              <div id="pl_estudios" class="tab-pane fade">
                <h3>Plan de Estudios</h3>
                <p>Some content in menu 1.</p>
              </div>
              <div id="docentes" class="tab-pane fade">
                <h3>Docentes</h3>
                <p>Some content in menu 2.</p>
              </div>
              <div id="inversion" class="tab-pane fade">
                <h3>Inversion</h3>
                <p>Some content in menu 2.</p>
              </div>
              <div id="inscripcion" class="tab-pane fade">
                <h3>Inscripcion</h3>
                <p>Some content in menu 2.</p>
              </div>
              <div id="info" class="tab-pane fade">
                <h3>Info</h3>
                <p>Some content in menu 2.</p>
              </div>
              
            </div>   
               </div>
                
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="cuadro">
                <div style="background:#155c8f">
                   <center>
                        <h3 style="color:white">Realizado</h3>
                   </center>
                </div> 
                <center>
                    <div class="custom-module">
                    <h1 style="font-size:35px"><?php echo $fecha; ?></h1>
                    <hr>
                    <span style="color:#155c8f"><i class="fa fa-map-marker" ></i>
                    Universidad Nacional Agraria la Molina - Facultad de Pesqueria</span>
                    <hr>
                   </div>
                </center>
                 
                </div>
            </div>
             </div><!-- end row -->
            </div><!-- end container -->
            </section>
        </section>

    </div><!-- end wrapper -->
        <footer class="section footer noover">
       <?php include('footer.php') ?>
        </footer>
    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/custom.js"></script>

</body>
</html>