﻿<?php
/*
 * Aplicacion
 */
define("APP_FDR", "ueups/gestion/");
define("APP_FDRWEB", "ueups/");
define("APP_DIR", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDR);
define("APP_URL", "http://" . $_SERVER['SERVER_NAME'] ."/". APP_FDR);
define("APP_URL_MANUAL", "http://" . $_SERVER['SERVER_NAME'] . '/' . APP_FDR);
define("TAM_PAG_LISTADO", 20);
define("TAM_PAG_LISTADO_PEQUE", 10);
define("TAM_PAG_LISTADO_5", 5);
define("ETIQUETA_TITULO", 'Instituto Pacífico');

define("APP_DIRIMGSLIDER", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "img/slider/");
define("APP_DIRIMGPRESENCIAL", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "img/actualizacion_presencial/");
define("APP_DIRIMGONLINE", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "img/actualizacion_online/");
define("APP_DIRIMGCURSO", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "img/curso/");
define("APP_DIRPDFCURSO", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "img/pdf/");
define("APP_DIRGALERIA", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDRWEB . "gallery_mod/eventos/");
//tiempo de inactividad en minutos
define("TIMEOUT_SESSION", 23);
// maximo de intentos permitidos.
define("MAX_ATTEMPTS", 5);

define("APP_DIRIMGSLIDER2", "http://" . $_SERVER['SERVER_NAME'] . ":/" . APP_FDRWEB . "img/slider/");
define("APP_UPLOAD_PHP", '/ueups/upload_image_script.php');

/*
 * Base de Datos
*/
$dbType = "MYSQL";
$dbHost = "localhost";
$dbUser = "root";
$dbPass = "@Pes2018R3D3$";
$dbName = "bd_pacifico";